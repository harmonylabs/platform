<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// /password/forgot -> /forgot-password -> password.request             -> /password/forgot
// /password/email -> /forgot-password -> password.email                -> /
// /password/reset/{token} -> /reset-password/{token} -> password.reset
// /password/reset -> /reset-password -> password.update

use Harmony\Http\Controllers\AdminUserController;
use Harmony\Http\Controllers\DashboardController;
use Harmony\Http\Controllers\RoleController;
use Harmony\Http\Controllers\User\ForgotPasswordController;
use Harmony\Http\Controllers\User\LoginController;
use Harmony\Http\Controllers\User\ResetPasswordController;
use Harmony\Http\Controllers\UserGroupController;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['web'],
    'prefix' => config('platform.prefix'),
    'namespace' => 'Harmony\Http\Controllers\User',
    'as' => 'admin.',
], function () {
    // login / logout
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('login', [LoginController::class, 'login'])->name('login.post');
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');

    // reset password
    Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');

    Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
    Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');
});

Route::group([
    'middleware' => ['web', 'admin.auth:admin', 'permission'],
    'prefix' => config('platform.prefix'),
    'as' => 'admin.',
    'namespace' => 'Harmony\Http\Controllers',
], function () {
    // dashboard
    Route::get('', [DashboardController::class, 'index'])->name('dashboard');

    Route::post('admin-user-image-upload', 'AdminUserController@upload')->name('admin-user-image-upload');

    // resources
    Route::resource('admin-user', 'AdminUserController')->except('show');
    Route::resource('role', 'RoleController')->except('show');
    Route::resource('user-group', 'UserGroupController')->except('show');
});
