const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableNotifications();

mix.options({
    terser: {
        terserOptions: {
            compress: {
                drop_console: true,
            },
        },
    },
})
    .setPublicPath('public')
    .js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css/app.css', [
        require('tailwindcss')
    ])
    .copy('resources/img', 'public/img')
    .webpackConfig({
        output: {
            chunkFilename: mix.inProduction() ? "public/js/[name].[chunkhash].js" : "public/js/[name].js "
        },
        resolve: {
            alias: {
                '@': path.resolve('resources/js')
            }
        }
    })
    .version()
    .sourceMaps();

if (mix.inProduction()) {
    mix.version();
}
