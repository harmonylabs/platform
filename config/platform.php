<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Route Prefix
    |--------------------------------------------------------------------------
    |
    | This prefix method can be used for the prefix of each route in the
    | administration panel. For example, you can change to 'admin'.
    |
    */

    'prefix' => 'admin',

    /*
    |--------------------------------------------------------------------------
    | Configurations for the user
    |--------------------------------------------------------------------------
    |
    | User configuration to manage user access.
    |
    */

    'user' => [
        'model' => Harmony\Database\Models\Customer::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Filesystems
    |--------------------------------------------------------------------------
    |
    | Here, you may specify the default filesystem disks that should be used
    | by Sentrio. The "local" disk, as well as variety of cloud based disks are
    | available to you.
    |
    */

    'filesystems' => [
        'disks' => [
            'platform' => [
                'driver' => 'local',
                'root' => storage_path('app/public'),
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider that defines how the
    | users are actually retrieved out of the database or other storage
    | mechanisms used by the application to persist your users data.
    */

    'auth' => [

        /*
        |--------------------------------------------------------------------------
        | Authentication Guards
        |--------------------------------------------------------------------------
        |
        | You may define every authentication guard for your application. The
        | configuration uses session storage and the Eloquent user provider.
        |
        */

        'guards' => [
            'admin' => [
                'driver' => 'session',
                'provider' => 'admin-users',
            ],

            'customer' => [
                'driver' => 'session',
                'provider' => 'customers',
            ],

            'admin_api' => [
                'driver' => 'passport',
                'provider' => 'admin-users',
                'hash' => false,
            ],
        ],

        /*
        |--------------------------------------------------------------------------
        | User Providers
        |--------------------------------------------------------------------------
        |
        | All authentication drivers have a user provider. That defines how the users
        | are actually retrieved out of the database or other storage mechanisms used
        | by this platform to persist your users data
        |
        */

        'providers' => [
            'admin-users' => [
                'driver' => 'eloquent',
                'model' => Harmony\Database\Models\AdminUser::class,
            ],

            'customers' => [
                'driver' => 'eloquent',
                'model' => Harmony\Database\Models\Customer::class,
            ],
        ],

        /*
        |--------------------------------------------------------------------------
        | Resetting Passwords
        |--------------------------------------------------------------------------
        |
        | You may specify multiple password reset configurations if you have more
        | than one user table or model in the application and you want to have
        | separate password reset settings based on the specific user type.
        |
        | The expire time is the number of minutes that each reset token will be
        | considered valid.
        |
        */

        'passwords' => [
            'adminusers' => [
                'provider' => 'admin-users',
                'table' => 'admin_password_resets',
                'expire' => 60,
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Image
    |--------------------------------------------------------------------------
    |
    |
    */

    'image' => [],

    'cart' => [
        'session_key' => 'cart_products',
        'promo_key' => 'cart_discount',
    ],

];
