<div align="center"><img src="/public/img/logo.svg" alt="Harmony Platform" /></div><br>

<div align="center">
  <img src="https://img.shields.io/badge/license-MIT-brightgreen.svg?style=for-the-badge" alt="LICENSE" />
  <img src="https://img.shields.io/travis/harmonylabs/platform.svg?style=for-the-badge" alt="Travis CI" />
  <img src="https://img.shields.io/packagist/dt/cartel/platform.svg?style=for-the-badge" alt="Total Downloads" />
  <img src="https://gitlab.styleci.io/repos/36750906/shield?style=for-the-badge" alt="Style CI" />
</div>

## About

Harmony Platform contains the core features of the Harmony platform eCommerce package.

## Server Requirements

The Harmony platform has a few system requirements. The minimum supported [Laravel](https://laravel.com) version is v5.8.

## Installation

You may use Composer to install Harmony Platform into your Laravel project:

```bash
composer require harmony/platform
```

After installing Platform, publisn it's assets using the ```harmony:install``` Artisan command:

```php
php artisan harmony:install
```

## Testing

Run the tests with:

```bash
vendor/bin/phpunit
```

## License

The MIT License (MIT). Please see [License File](/LICENSE.md) for more information.
