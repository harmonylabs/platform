<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Harmony Platform eCommerce') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="shortcut icon" href="{{ asset('vendor/platform/img/favicon.png') }}">

        <!-- Styles -->
        @if (file_exists(public_path('mix-manifest.json')))
            <link href="{{ mix('vendor/platform/css/app.css') }}" rel="stylesheet">
        @else
            <link href="{{ asset('vendor/platform/css/app.css') }}" rel="stylesheet">
        @endif
    </head>
    <body>
        <div id="app">

            <vh-alert></vh-alert>
            <vh-confirm></vh-confirm>
            <vh-layout inline-template>

                <div class="flex items-start">
                    <div :class="sidebar ? 'w-16 z-0 transition sidebar-collapsed duration-500' : 'w-64'">
                        @include('platform::partials.sidebar')
                    </div>

                    <div class="w-full">
                        <div class="w-full">
                            @include('platform::partials.header')
                            @include('platform::partials.flash')
                            @include('platform::partials.breadcrumb')

                            <h1 class="mx-4 my-3">
                                @yield('page_title')
                            </h1>

                            <div class="rounded p-5 mx-3 my-3 bg-white">
                                {{-- <router-view></router-view> --}}
                                @yield('content')
                            </div>

                            @include('platform::partials.footer')
                        </div>
                    </div>
                </div>
            </vh-layout>
        </div>

        <!-- Scripts -->
        @if (file_exists(public_path('mix-manifest.json')))
            <script src="{{ mix('vendor/platform/js/platform.js') }}" defer></script>
        @else
            <script src="{{ asset('vendor/platform/js/platform.js') }}" defer></script>
        @endif

        @stack('scripts')

        @if (file_exists(public_path('mix-manifest.json')))
            <script src="{{ mix('vendor/platform/js/app.js') }}" defer></script>
        @else
            <script src="{{ asset('vendor/platform/js/app.js') }}" defer></script>
        @endif
    </body>
</html>
