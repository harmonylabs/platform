<footer class="text-gray-700 body-font">
    <div class="container px-5 py-8 mx-auto flex items-center sm:flex-row flex-col">
        <a class="flex title-font font-medium items-center md:justify-start justify-center text-gray-900">
            <img src="{{ asset('vendor/platform/img/logo-only.svg') }}" class="h-10 w-10" />
        </a>

        <p class="text-sm text-gray-500 sm:pl-4 sm:border-l-2 sm:border-gray-200 sm:py-2 sm:mt-0 mt-4">
            &copy; {{ date('Y') }} — Harmony
        </p>
    </div>
</footer>
