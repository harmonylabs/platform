@extends('platform::layouts.app')

@section('page_title')
    <div class="flex items-center text-gray-800">
        <div class="text-xl font-semibold text-red-700">
            {{ __('User Group List') }}
        </div>

        <div class="ml-auto">
            <a href="{{ route('admin.user-group.create') }}" class="px-4 py-2 font-semibold leading-7 text-white bg-red-600 rounded hover:text-white hover:bg-red-700">
                <svg class="inline-block w-5 h-5 text-white" fill="currentColor" viewBox="0 0 24 24">
                    <path d="M17 11a1 1 0 0 1 0 2h-4v4a1 1 0 0 1-2 0v-4H7a1 1 0 0 1 0-2h4V7a1 1 0 0 1 2 0v4h4z"/>
                </svg>
                {{ __('Create') }}
            </a>
        </div>
    </div>
@endsection

@section('content')
    <div>
        <user-group-table></user-group-table>
    </div>
@endsection
