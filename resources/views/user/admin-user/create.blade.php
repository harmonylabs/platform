@extends('platform::layouts.app')

@section('page_title')
    <div class="flex items-center text-gray-800">
        <div class="text-xl font-semibold text-red-700">
            {{ __('Admin User Create') }}
        </div>
    </div>
@endsection

@section('content')
    <div class="flex items-center">
        <admin-user-save base-url="{{ asset(config('platform.prefix')) }}" inline-template>
            <div class="block w-full">
                <form method="post" action="{{ route('admin.admin-user.store') }}">
                    @csrf
                </form>
            </div>
        </admin-user-save>
    </div>
@endsection
