<div class="flex w-full mt-3">
    <vh-input>
        label-text="{{ __('First Name') }}"
        field-name="first_name"
        init-value="{{ $adminUser->first_name ?? '' }}"
        error-text="{{ $errors->first('first_name') }}"
    ></vh-input>
</div>

<div class="flex w-full mt-3">
    <vh-input
        label-text="{{ __('Last Name') }}"
        field-name="last_name"
        init-value="{{ $adminUser->last_name ?? '' }}"
        error-text="{{ $errors->first('last_name') }}"
    ></vh-input>
</div>
