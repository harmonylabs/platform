@extends('platform::layouts.app')

@section('page_title')
    <div class="text-gray-800 flex items-center">
        <div class="text-xl text-red-700 font-semibold">
            {{ __('Admin User Edit') }}
        </div>
    </div>
@endsection

@section('content')
    <div class="flex items-center">
        <admin-user-save base-url="{{ asset(config('platform.prefix')) }}" :admin-user="{{ $adminUser }}" inline-template>
            <div class="w-full">
                <form method="post" action="{{ route('admin.admin-user.update', $adminUser->id) }}">
                    @csrf
                    @method('put')

                    <vh-tabs>
                        @foreach ($tabs as $tab)
                            <vh-tab identifier="{{ $tab->key() }}" name="{{ $tab->label() }}">
                                @php
                                    $path = $tab->view();
                                @endphp

                                @include($path)
                            </vh-tab>
                        @endforeach
                    </vh-tabs>

                    <div class="mt-3 py-3">
                        <button type="submit" class="px-6 py-3 font-semibold leading-7 text-white hover:text-white bg-red-600 rounded hover:bg-red-700">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 inline-flex w-4" fill="currentColor" viewBox="0 0 20 20">
                                <path d="M0 2C0 .9.9 0 2 0h14l4 4v14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm5 0v6h10V2H5zm6 1h3v4h-3V3z"/>
                            </svg>

                            <span class="ml-3">{{ __('Save') }}</span>
                        </button>

                        <a href="{{ route('admin.admin-user.index') }}" class="px-6 py-3 font-semibold inline-block text-white leading-7 hover:text-white bg-gray-500 rounded hover:bg-gray-600">
                            <span class="leading-7">
                                {{ __('Cancel') }}
                            </span>
                        </a>
                    </div>
                </form>
            </div>
        </admin-user-save>
    </div>
@endsection
