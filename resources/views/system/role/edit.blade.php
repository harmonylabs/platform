@extends('platform::layouts.app')

@section('meta_title')
    {{ __('Harmony Platform eCommerce') }}
@endsection

@section('page_title')
    <div class="flex items-center text-gray-800">
        <div class="text-xl font-semibold text-red-700">
            {{ __('Role Edit') }}
        </div>
    </div>
@endsection

@section('content')
    <div class="flex items-center">
        <role-save base-url="{{ asset(config('platform.prefix')) }}" role="{{ $role }}" inline-template>
            <div class="block w-full">
                <form method="post" action="{{ route('admin.role.update', $role->id) }}" @submit="handleSubmit">
                    @csrf
                    @method('put')

                    <vh-tabs>
                        @foreach ($tabs as $tab)
                            <vh-tab identifier="{{ $tab->key() }}" name="{{ $tab->label() }}">
                                @php
                                    $path = $tab->view();
                                @endphp
                                @include($path)
                            </vh-tab>
                        @endforeach
                    </vh-tabs>

                    <div class="py-3 mt-3">
                        <button type="submit" class="px-6 py-3 font-semibold leading-7 text-white bg-red-600 rounded hover:text-white hover:bg-red-700">
                            <svg xmlns="http://www.w3.org/2000/svg" class="inline-flex w-4 h-4" fill="currentColor" viewBox="0 0 20 20">
                                <path d="M0 2C0 .9.9 0 2 0h14l4 4v14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm5 0v6h10V2H5zm6 1h3v4h-3V3z"/>
                            </svg>
                            <span class="ml-3">Save</span>
                        </button>

                        <a href="{{ route('admin.role.index') }}" class="inline-block px-6 py-3 font-semibold leading-7 text-white bg-gray-500 rounded hover:text-white hover:bg-gray-600">
                            <span class="leading-7">
                                Cancel
                            </span>
                        </a>
                    </div>
                </form>
            </div>
        </role-save>
    </div>
@endsection
