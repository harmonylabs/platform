<div class="mt-3 flex w-full">
    <vh-input
        label-text="{{ __('Name') }}"
        field-name="name"
        init-value="{{ $role->name ?? '' }}"
        error-text="{{ $errors->first('name') }}"
    ></vh-input>
</div>

<div class="mt-3 flex w-full">
    <vh-input
        label-text="{{ __('Description') }}"
        field-name="description"
        init-value="{{ $role->description ?? '' }}"
        error-text="{{ $errors->first('description') }}"
    ></vh-input>
</div>

<div class="flex flex-wrap">
    @foreach ($permissions as $group)
        <div class="mt-3 w-1/4">
            <div class="ml-3 rounded border">
                <div class="p-5 border-b">
                    {{ $group->label() }}
                </div>
                <div class="p-5">
                    @foreach ($group->collections as $permission)
                        <div class="mt-1 flex w-full">
                            <vh-toggle
                                label-text="{{ $permission->label() }}"
                                field-name="permissions[{{ $permission->routes() }}]"
                                init-value="{{ (isset($role) && $role->hasPermission($permission->routes())) ? 1 : 0 }}"
                            ></vh-toggle>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</div>
