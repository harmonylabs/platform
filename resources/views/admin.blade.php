@extends('platform::layouts.app')

@section('meta_title')
    Harmony Platform eCommerce Admin Dashboard
@endsection

@section('page_title')
    <div class="flex items-center text-gray-800">
        <div class="text-xl font-semibold text-red-700">
            Dashboard
        </div>
    </div>
@endsection

@section('content')
    <div class="flex justify-around my-5">
        {!! $orderWidget->render() !!}
        {!! $customerWidget->render() !!}
        {!! $revenueWidget->render() !!}
    </div>

    <div class="flex justify-around my-5 mt-5">
        <div class="w-full bg-white border border-gray-400 rounded">
            <div class="p-4 font-semibold border-b text-md">
                Admin Dashboard
            </div>
        </div>
    </div>
@endsection
