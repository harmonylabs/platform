window.Vue = require('vue');

window.Platform = (function () {
    return {
        initialize: function (callback) {
            callback(window.Vue)
        }
    };
})();

window.EventBus = new Vue();

exports = module.exports = Platform;
