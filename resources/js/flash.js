import VHFlash from './components/Flash.vue';

export default {
    install(Vue, args = {}) {
        if (this.installed) return;

        this.installed = true;
        this.params = args;

        Vue.component('vh-flash', VHFlash);

        const flash = params => {
            EventBus.$emit('notify', params);
        }

        Vue.prototype.$flash = flash;
        Vue['$flash'] = flash;
    }
}
