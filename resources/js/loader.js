import Vue from 'vue'

const components = require.context('./components', true, /\/[a-zA-Z0-9]+\.vue$/)
components.keys().forEach(el => {
    const name = String(components(el).default.name).toLowerCase()
    Vue.component(`x-${name}`, components(el).default)
})
