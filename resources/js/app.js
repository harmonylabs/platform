window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

import './platform';
import Router from './router';
import Store from './store';
import VueHarmony from 'vue-harmony';

import Vddl from 'vddl';
import HeroIcon from 'vue-heroicons';

Vue.use(VueHarmony);
Vue.use(Vddl);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('order-table', require('./components/order/OrderTable.vue').default);

Vue.component('user-group-table', require('./components/user-group/UserGroupTable.vue').default);
Vue.component('user-group-save', require('./components/user-group/UserGroupSave.vue').default);

// attribute
// product
// currency
// category

Vue.component('configuration-save', require('./components/configuration/ConfigurationSave.vue').default);

// menu
// page
// order status

Vue.component('role-table', require('./components/role/RoleTable.vue').default);
Vue.component('role-save', require('./components/role/RoleSave.vue').default);

Vue.component('admin-user-table', require('./components/admin-user/AdminUserTable.vue').default);
Vue.component('admin-user-save', require('./components/admin-user/AdminUserSave.vue').default);

Vue.component('vh-layout', require('./components/Layout.vue').default);
Vue.component('vh-flash', require('./components/Flash.vue').default);
Vue.component('login-section', require('./components/LoginSection.vue').default);
Vue.component('password-reset', require('./components/PasswordReset.vue').default);
Vue.component('password-new', require('./components/PasswordNew.vue').default);

Vue.component('heroicon', HeroIcon);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router: Router,
    Store: Store
});
