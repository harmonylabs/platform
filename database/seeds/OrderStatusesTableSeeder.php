<?php

use Harmony\Database\Contracts\OrderStatusRepository;
use Illuminate\Database\Seeder;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * The order status repository implementation.
     *
     * @var \Harmony\Database\Contracts\OrderStatusRepository
     */
    protected $orderStatus;

    /**
     * Create a new seeder instance.
     *
     * @param  \Harmony\Database\Contracts\OrderStatusRepository  $orderStatus
     * @return void
     */
    public function __construct(OrderStatusRepository $orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = $this->orderStatus->create(['name' => 'Pending']);
        $status->is_default = 1;
        $status->save();

        $this->orderStatus->create(['name' => 'Processing']);
        $this->orderStatus->create(['name' => 'Completed']);
    }
}
