<?php

use Harmony\Database\Contracts\CurrencyRepository;
use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * The currency repository implementation.
     *
     * @var \Harmony\Database\Contracts\CurrencyRepository
     */
    protected $currency;

    /**
     * Create a new seeder instance.
     *
     * @param  \Harmony\Database\Contracts\CurrencyRepository  $currency
     * @return void
     */
    public function __construct(CurrencyRepository $currency)
    {
        $this->currency = $currency;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->currency->create([
            'name' => 'US Dollar',
            'code' => 'usd',
            'symbol' => '$',
            'conversion_rate' => 1,
            'status' => 'ENABLED',
        ]);
    }
}
