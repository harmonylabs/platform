<?php

use Harmony\Database\Contracts\RoleRepository;
use Harmony\Database\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * The role repository implementation.
     *
     * @var \Harmony\Database\Contracts\RoleRepository
     */
    protected $role;

    /**
     * Create a new seeder instance.
     *
     * @param  \Harmony\Database\Contracts\RoleRepository  $role
     * @return void
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['name' => Role::ADMIN];

        $this->role->create($data);
    }
}
