<?php

use Harmony\Database\Contracts\UserGroupRepository;
use Illuminate\Database\Seeder;

class UserGroupsTableSeeder extends Seeder
{
    /**
     * The user group repository implementation.
     *
     * @var \Harmony\Database\Contracts\UserGroupRepository
     */
    protected $userGroup;

    /**
     * Create a new seeder instance.
     *
     * @param  \Harmony\Database\Contracts\UserGroupRepository  $userGroup
     * @return void
     */
    public function __construct(UserGroupRepository $userGroup)
    {
        $this->userGroup = $userGroup;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->userGroup->create([
            'name' => 'Default Group',
            'is_default' => 1,
        ]);
    }
}
