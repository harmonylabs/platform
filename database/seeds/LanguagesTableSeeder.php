<?php

use Harmony\Database\Contracts\LanguageRepository;
use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * The language repository implementation.
     *
     * @var \Harmony\Database\Contracts\LanguageRepository
     */
    protected $language;

    /**
     * Create a new seeder instance.
     *
     * @param  \Harmony\Database\Contracts\LanguageRepository  $language
     */
    public function __construct(LanguageRepository $language)
    {
        $this->language = $language;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->language->create([
            'name' => 'English',
            'code' => 'en',
            'is_default' => 1,
        ]);
    }
}
