<?php

use Harmony\Database\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = __DIR__.'/countries.json';

        $countries = json_decode(file_get_contents($path), true);
        foreach ($countries as $country) {
            Country::create([
                'code' => strtolower(Arr::get($country, 'alpha2Code')),
                'name' => Arr::get($country, 'name'),
                'phone_code' => Arr::get($country, 'callingCodes.0'),
                'currency_code' => Arr::get($country, 'currencies.0.code'),
                'currency_symbol' => Arr::get($country, 'currencies.0.symbol'),
                'lang_code' => Arr::get($country, 'languages.0.name'),
            ]);
        }
    }
}
