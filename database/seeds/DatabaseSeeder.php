<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CountriesTableSeeder::class,
            CurrenciesTableSeeder::class,
            LanguagesTableSeeder::class,
            OrderStatusesTableSeeder::class,
            RolesTableSeeder::class,
            UserGroupsTableSeeder::class,
        ]);
    }
}
