<?php

// use Cartel\Database\Models\Country;
// use Illuminate\Support\Arr;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->default(null);
            $table->string('code')->nullable()->default(null);
            $table->string('phone_code')->nullable()->default(null);
            $table->string('currency_code')->nullable()->default(null);
            $table->string('currency_symbol')->nullable()->default(null);
            $table->string('lang_code')->nullable()->default(null);
            $table->timestamps();
        });

        // $path = __DIR__.'/../seeds/countries.json';
        // $countries = json_decode(file_get_contents($path), true);
        // foreach ($countries as $country) {
        //     Country::create([
        //         'code' => strtolower(Arr::get($country, 'alpha2Code')),
        //         'name' => Arr::get($country, 'name'),
        //         'phone_code' => Arr::get($country, 'callingCodes.0'),
        //         'currency_code' => Arr::get($country, 'currencies.0.code'),
        //         'currency_symbol' => Arr::get($country, 'currencies.0.symbol'),
        //         'lang_code' => Arr::get($country, 'languages.0.name')
        //     ]);
        // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
