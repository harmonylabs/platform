<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Harmony\Database\Models\Country;

$factory->define(Country::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
        'code' => $faker->countryCode,
        'phone_code' => $faker->currencyCode,
        'currency_code' => $faker->currencyCode,
        'currency_symbol' => '$',
        'lang_code' => $faker->languageCode,
    ];
});
