<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Harmony\Database\Models\Attribute;
use Illuminate\Support\Str;

$factory->define(Attribute::class, function (Faker $faker) {
    $name = $faker->sentence;

    return [
        'name' => $name,
        'slug' => Str::slug($name),
    ];
});
