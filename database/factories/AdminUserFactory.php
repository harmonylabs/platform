 <?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Harmony\Database\Models\AdminUser;
use Harmony\Database\Models\Role;

$factory->define(AdminUser::class, function (Faker $faker) {
    $role = factory(Role::class)->create();

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => 'secret',
        // 'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'role_id' => $role->id,
        'is_super_admin' => rand(0, 1),
        'image_path' => null,
    ];
});
