<?php

namespace Harmony\Tests\Feature;

use Harmony\Http\Controllers\User\LoginController;
use Harmony\Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function redirect_path_for_login_controller_test()
    {
        $loginController = app(LoginController::class);
        $adminPath = route('admin.dashboard');

        $this->assertEquals($loginController->redirectPath(), $adminPath);
    }
}
