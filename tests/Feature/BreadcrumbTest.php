<?php

namespace Harmony\Tests\Feature;

use Harmony\Breadcrumbs\Builder;
use Harmony\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BreadcrumbTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_build_breadcrumb_test()
    {
        $builder = new Builder;
        $builder->make('test.route', function ($breadcrumb) {
            $breadcrumb->route('test.route');
        });

        $testRoute = $builder->get('test.route');
        $this->assertEquals($testRoute->route(), 'test.route');
    }

    /** @test */
    public function it_creates_breadcrumb_label_test()
    {
        $builder = new Builder;
        $builder->make('test.route', function ($breadcrumb) {
            $breadcrumb->label('test label');
        });

        $testLabel = $builder->get('test.route');

        $this->assertEquals($testLabel->label(), 'test label');
    }

    /** @test */
    public function it_creates_parent_breadcrumb_test()
    {
        $builder = new Builder;
        $builder->make('test.route', function ($breadcrumb) {
            $breadcrumb->label('test parent label');
        });

        $builder->make('test.child.route', function ($breadcrumb) {
            $breadcrumb->label('test child label');
            $breadcrumb->parent('test.route');
        });

        $test = $builder->get('test.child.route');

        $this->assertEquals($test->parents->count(), 1);
    }
}
