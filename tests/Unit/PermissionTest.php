<?php

namespace Harmony\Platform\Tests\Unit;

use Harmony\Database\Models\AdminUser;
use Harmony\Database\Models\Permission;
use Harmony\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;

class PermissionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function permission_test()
    {
        $this->createAdminUser(['is_super_admin' => 0])
            ->actingAs($this->user, 'admin')
            ->get(route('admin.dashboard'))
            ->assertStatus(Response::HTTP_FORBIDDEN);

        $this->createPermissionForUser($this->user, 'admin.dashboard');

        $this->actingAs($this->user, 'admin')
            ->get(route('admin.dashboard'))
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * Create a permission for the user.
     *
     * @param  \Harmony\Database\Models\AdminUser  $user
     * @param  string  $name
     * @return void
     */
    protected function createPermissionForUser(AdminUser $user, string $name)
    {
        $permission = new Permission(['name' => $name]);

        $user->role->permissions()->save($permission);
        $user->load('role.permissions');
    }
}
