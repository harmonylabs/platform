<?php

namespace Harmony\Tests\Controllers;

use Harmony\Database\Models\AdminUser;
use Harmony\Database\Models\Role;
use Harmony\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminUserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function is_admin_user_index_route_test()
    {
        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->get(route('admin.admin-user.index'))
            ->assertStatus(200);
        // ->assertSee(__('Admin User List'));
    }

    /** @test */
    public function is_admin_user_create_route_test()
    {
        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->get(route('admin.admin-user.create'))
            ->assertStatus(200);
        // ->assertSee('Admin User Create');
    }

    /** @test */
    public function is_admin_user_store_route_test()
    {
        $role = factory(Role::class)->create();

        $data = [
            'first_name' => 'test admin-user name',
            'last_name' => 'test-admin-user-name',
            'is_super_admin' => 0,
            'email' => $this->faker->email,
            'role_id' => $role->id,
            'password' => 'randompassword',
            'password_confirmation' => 'randompassword',
            'language' => 'en',
        ];

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->post(route('admin.admin-user.store', $data))
            ->assertRedirect(route('admin.admin-user.index'));

        $this->assertDatabaseHas('admin_users', ['first_name' => 'test admin-user name']);
    }

    /** @test */
    public function is_admin_user_edit_route_test()
    {
        $adminUser = factory(AdminUser::class)->create();

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->get(route('admin.admin-user.edit', $adminUser->id))
            ->assertStatus(200);
        // ->assertSee(__('Admin User Edit'));
    }

    /** @test */
    public function is_admin_user_update_route_test()
    {
        $adminUser = factory(AdminUser::class)->create();
        $adminUser->first_name = 'updated admin-user name';
        $adminUser->language = 'en';
        $data = $adminUser->toArray();

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->put(route('admin.admin-user.update', $adminUser->id), $data)
            ->assertRedirect(route('admin.admin-user.index'));

        $this->assertDatabaseHas('admin_users', ['first_name' => 'updated admin-user name']);
    }

    /** @test */
    public function is_admin_user_destroy_route_test()
    {
        $adminUser = factory(AdminUser::class)->create();

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->delete(route('admin.admin-user.destroy', $adminUser->id))
            ->assertStatus(200);

        $this->assertDatabaseMissing('admin_users', ['id' => $adminUser->id]);
    }
}
