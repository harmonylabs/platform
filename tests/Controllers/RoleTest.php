<?php

namespace Harmony\Tests\Controllers;

use Harmony\Database\Models\Role;
use Harmony\Support\Facades\Permission;
use Harmony\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoleTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function is_role_index_route_test()
    {
        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->get(route('admin.role.index'))
            ->assertStatus(200);
        // ->assertSee(__('Role List'));
    }

    /** @test */
    public function is_role_create_route_test()
    {
        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->get(route('admin.role.create'))
            ->assertStatus(200);
        // ->assertSee(__('Role Create'));
    }

    /** @test */
    public function is_role_store_route_test()
    {
        $data = ['name' => 'test role name'];

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->post(route('admin.role.store', $data))
            ->assertRedirect(route('admin.role.index'));

        $this->assertDatabaseHas('roles', ['name' => 'test role name']);
    }

    /** @test */
    public function is_role_edit_route_test()
    {
        $role = factory(Role::class)->create();

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->get(route('admin.role.edit', $role->id))
            ->assertStatus(200);
        // ->assertSee(__('Role Edit'));
    }

    /** @test */
    public function is_role_update_route_test()
    {
        $role = factory(Role::class)->create();
        $role->name = 'updated role name';
        $data = $role->toArray();

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->put(route('admin.role.update', $role->id), $data)
            ->assertRedirect(route('admin.role.index'));

        $this->assertDatabaseHas('roles', ['name' => 'updated role name']);
    }

    /** @test */
    public function is_role_destroy_route_test()
    {
        $role = factory(Role::class)->create();

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->delete(route('admin.role.destroy', $role->id))
            ->assertStatus(200);

        $this->assertDatabaseMissing('roles', ['id' => $role->id]);
    }

    /** @test */
    public function is_role_store_with_permissions_route_test()
    {
        $permissions = Permission::all()->first()->groups;
        $collection = collect();

        foreach ($permissions as $permission) {
            $collection->push($permission->routes());
        }

        $data = [
            'name' => 'test role name',
            'permissions' => $collection->toArray(),
        ];

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->post(route('admin.role.store', $data))
            ->assertRedirect(route('admin.role.index'));

        $this->assertDatabaseHas('roles', ['name' => 'test role name']);
    }

    /** @test */
    public function is_role_update_with_permissions_route_test()
    {
        $permissions = Permission::all()->first()->groups;
        $collection = collect();

        foreach ($permissions as $permission) {
            $collection->push($permission->routes());
        }

        $role = factory(Role::class)->create();
        $role->name = 'updated role name';
        $data = $role->toArray();
        $data['permissions'] = $collection->toArray();

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->put(route('admin.role.update', $role->id), $data)
            ->assertRedirect(route('admin.role.index'));

        $this->assertDatabaseHas('roles', ['name' => 'updated role name']);
    }
}
