<?php

namespace Harmony\Tests\Controllers;

use Harmony\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConfigurationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function is_configuration_index_route_test()
    {
        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->get(route('admin.configuration.index'))
            ->assertStatus(200);
            // ->assertSee(__('Basic Settings'));
    }

    /** @test */
    public function is_category_store_route_test()
    {
        $data = ['site' => 'test site title'];

        $this->createAdminUser()
            ->actingAs($this->user, 'admin')
            ->post(route('admin.configuration.store', $data))
            ->assertRedirect(route('admin.configuration.index'));

        $this->assertDatabaseHas('configurations', ['code' => 'site_title']);
    }
}
