<?php

namespace Harmony\Tests;

use Faker\Generator as Faker;
use Harmony\Database\Contracts\CurrencyRepository;
use Harmony\Database\Models\AdminUser;
use Harmony\Database\Models\Currency;
use Harmony\PlatformServiceProvider;
use Illuminate\Support\Facades\Notification;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    /**
     * The admin user instance.
     *
     * @var \Harmony\Database\Models\AdminUser
     */
    protected $user;

    /**
     * The faker instance.
     *
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setup(): void
    {
        parent::setUp();

        $this->faker = $this->app->make(Faker::class);

        $this->withFactories(__DIR__.'/../database/factories');

        $this->setUpDatabase();
        $this->setDefaultCurrency();

        Notification::fake();
    }

    /**
     * Get package providers.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            PlatformServiceProvider::class,
        ];
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('app.key', 'base64:UTyp33UhGolgzCK5CJmT+hNHcA+dJyp3+oINtX+VoPI=');
    }

    /**
     * Get package aliases.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getPackageAliases($app)
    {
        return [
            'Breadcrumb' => \Harmony\Support\Facades\Breadcrumb::class,
            'Menu' => \Harmony\Support\Facades\Menu::class,
            'Permission' => \Harmony\Support\Facades\Permission::class,
            'Tabs' => \Harmony\Support\Facades\Tabs::class,
            'Widget' => \Harmony\Support\Facades\Widget::class,
        ];
    }

    /**
     * Setup the database.
     *
     * @return void
     */
    protected function setUpDatabase(): void
    {
        $this->loadLaravelMigrations();
        $this->resetDatabase();
    }

    /**
     * Reset the database.
     *
     * @return void
     */
    protected function resetDatabase(): void
    {
        $this->artisan('migrate');
    }

    protected function setDefaultCurrency()
    {
        factory(Currency::class)->create();

        $currencyRepository = app(CurrencyRepository::class);
        $currency = $currencyRepository->all()->first();

        $this->withSession(['default_currency' => $currency]);
    }

    /**
     * Create test admin user.
     *
     * @param  array  $data
     * @return self
     */
    protected function createAdminUser($data = ['is_super_admin' => 1]): self
    {
        if (null === $this->user) {
            $this->user = factory(AdminUser::class)->create($data);
        }

        return $this;
    }
}
