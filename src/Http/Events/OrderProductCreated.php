<?php

namespace Harmony\Http\Events;

use Harmony\Database\Models\OrderProduct;
use Illuminate\Queue\SerializesModels;

class OrderProductCreated
{
    use SerializesModels;

    /**
     * The order product model.
     *
     * @var \Harmony\Database\Models\OrderProduct
     */
    public $orderProduct;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(OrderProduct $orderProduct)
    {
        $this->orderProduct = $orderProduct;
    }
}
