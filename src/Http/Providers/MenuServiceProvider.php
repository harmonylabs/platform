<?php

namespace Harmony\Http\Providers;

use Harmony\Menu\Builder;
use Harmony\Menu\Item;
use Harmony\Support\Facades\Menu;
use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();

        $this->app->alias('menu', 'Harmony\Menu\Builder');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootMenus();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['menu', 'Harmony\Menu\Builder'];
    }

    /**
     * Register the provider service.
     *
     * @return void
     */
    protected function registerService()
    {
        $this->app->singleton('menu', function () {
            return new Builder();
        });
    }

    /**
     * Bootstrap the package provider
     *
     * @return void
     */
    protected function bootMenus()
    {
        // Menu::make('catalog', function (Item $menu) {
        //     $menu->label('Catalog')
        //         ->type(Item::ADMIN)
        //         ->icon('store-front')
        //         ->route('#');
        // });

        // $catalog = Menu::get('catalog');
        // $catalog->subMenu('product', function (Item $menu) {
        //     $menu->key('product')
        //         ->type(Item::ADMIN)
        //         ->label('Product')
        //         ->route('admin.product.index');
        // });
        // $catalog->subMenu('category', function (Item $menu) {
        //     $menu->key('category')
        //         ->type(Item::ADMIN)
        //         ->label('Category')
        //         ->route('admin.category.index');
        // });
        // $catalog->subMenu('attribute', function (Item $menu) {
        //     $menu->key('attribute')
        //         ->type(Item::ADMIN)
        //         ->label('Attribute')
        //         ->route('admin.attribute.index');
        // });

        // // cms / content
        // Menu::make('content', function (Item $menu) {
        //     $menu->label('Content')
        //         ->type(Item::ADMIN)
        //         ->icon('news-paper')
        //         ->route('#');
        // });

        // $content = Menu::get('content');
        // $content->subMenu('menu-group', function (Item $menu) {
        //     $menu->key('menu-group')
        //         ->type(Item::ADMIN)
        //         ->label('Menu')
        //         ->route('admin.menu-group.index');
        // });

        // order

        // user
        Menu::make('user', function (Item $menu) {
            $menu->label('User')
                ->type(Item::ADMIN)
                ->icon('user-group')
                ->route('#');
        });

        $userMenu = Menu::get('user');
        $userMenu->subMenu('admin-user', function (Item $menu) {
            $menu->key('admin-user')
                ->type(Item::ADMIN)
                ->label('Admin User')
                ->route('admin.admin-user.index');
        });
        $userMenu->subMenu('user_group', function (Item $menu) {
            $menu->key('user_group')
                ->type(Item::ADMIN)
                ->label('User Group')
                ->route('admin.user-group.index');
        });

        // system
        Menu::make('system', function (Item $menu) {
            $menu->label('System')
                ->type(Item::ADMIN)
                ->icon('cog')
                ->route('#');
        });
        $system = Menu::get('system');
        $system->subMenu('role', function (Item $menu) {
            $menu->key('role')
                ->type(Item::ADMIN)
                ->label('Role')
                ->route('admin.role.index');
        });
        // $system->subMenu('currency', function (Item $menu) {
        //     $menu->key('currency')
        //         ->type(Item::ADMIN)
        //         ->label('Currency')
        //         ->route('admin.currency.index');
        // });
    }
}
