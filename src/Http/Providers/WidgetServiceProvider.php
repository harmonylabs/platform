<?php

namespace Harmony\Http\Providers;

use Harmony\Support\Facades\Widget;
use Harmony\Widgets\TotalCustomer;
use Harmony\Widgets\TotalOrder;
use Harmony\Widgets\TotalRevenue;
use Harmony\Widgets\WidgetManager;
use Illuminate\Support\ServiceProvider;

class WidgetServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();

        $this->app->alias('widget', 'Harmony\Widgets\WidgetManager');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootWidgets();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['widget', 'Harmony\Widgets\WidgetManager'];
    }

    /**
     * Register the provider service.
     *
     * @return void
     */
    protected function registerService()
    {
        $this->app->singleton('widget', function ($app) {
            return new WidgetManager();
        });
    }

    /**
     * Register the widget manager.
     *
     * @return void
     */
    protected function bootWidgets()
    {
        $totalCustomer = new TotalCustomer;
        $totalOrder = new TotalOrder;
        $totalRevenue = new TotalRevenue;

        Widget::make($totalCustomer->identifier(), $totalCustomer);
        Widget::make($totalOrder->identifier(), $totalOrder);
        Widget::make($totalRevenue->identifier(), $totalRevenue);
    }
}
