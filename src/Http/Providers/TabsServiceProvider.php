<?php

namespace Harmony\Http\Providers;

use Harmony\Support\Facades\Tabs;
use Harmony\Tabs\Factory;
use Harmony\Tabs\Manager;
use Illuminate\Support\ServiceProvider;

class TabsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();

        $this->app->singleton('tabs', 'Harmony\Tabs\Manager');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootTabs();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['tabs', 'Harmony\Tabs\Manager'];
    }

    /**
     * Register the provider service.
     *
     * @return void
     */
    protected function registerService()
    {
        $this->app->singleton('tabs', function () {
            new Manager();
        });
    }

    protected function bootTabs()
    {
        Tabs::put('user.admin-user', function (Factory $tab) {
            $tab->key('user.admin-user.info');
            $tab->label('Basic Info');
            $tab->view('platform::user.admin-user._fields');
        });

        Tabs::put('user.user-group', function (Factory $tab) {
            $tab->key('user.user-group.info');
            $tab->label('Basic Info');
            $tab->view('platform::user.user-group._fields');
        });

        // system tabs
        Tabs::put('system.role', function (Factory $tab) {
            $tab->key('role.info');
            $tab->label('Basic Info');
            $tab->view('platform::system.role._fields');
        });
    }
}
