<?php

namespace Harmony\Http\Providers;

use Harmony\Database\Models\Order;
use Harmony\Http\Events\OrderProductCreated;
use Harmony\Http\Listeners\SaveOrderProduct;
use Harmony\Http\Observers\OrderObserver;
use Harmony\Http\Observers\UserObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        OrderProductCreated::class => [
            SaveOrderProduct::class,
        ],
    ];

    /**
     * Bootstrap any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $this->registerUserModelObserver();
    }

    /**
     * Register the user model observers.
     *
     * @return void
     */
    public function registerUserModelObserver()
    {
        $user = config('platform.user.model');

        try {
            $model = app($user);
        } catch (\Exception $e) {
            $model = null;
        }

        if ($model !== null) {
            $model->observe(UserObserver::class);
        }

        Order::observe(OrderObserver::class);
    }
}
