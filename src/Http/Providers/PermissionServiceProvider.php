<?php

namespace Harmony\Http\Providers;

use Harmony\Permission\Factory;
use Harmony\Permission\Group;
use Harmony\Permission\Manager;
use Harmony\Support\Facades\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();

        $this->app->singleton('permission', 'Harmony\Permission\Manager');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootPermissions();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['permission', 'Harmony\Permission\Manager'];
    }

    /**
     * Register the provider service.
     *
     * @return void
     */
    protected function registerService()
    {
        $this->app->singleton('permission', function () {
            new Manager();
        });
    }

    /**
     * Bootstrap the package provider
     *
     * @return void
     */
    protected function bootPermissions()
    {
        $group = Permission::addGroup('dashboard', function (Group $group) {
            $group->label('Dashboard');
        });
        $group->addPermission('admin-dashboard', function (Factory $permission) {
            $permission->label('Dashboard')
                ->routes('admin.dashboard');
        });

        // config group
        // product
        // order
        // category
        // language

        // admin-user
        $group = Permission::addGroup('admin-user', function (Group $group) {
            $group->label('Admin User Permission');
        });
        $group->addPermission('admin-admin-user-list', function (Factory $permission) {
            $permission->label('Admin User List')
                ->routes('admin.admin-user.index');
        });
        $group->addPermission('admin-admin-user-create', function (Factory $permission) {
            $permission->label('Admin User Create')
                ->routes('admin.admin-user.create,admin.admin-user.store');
        });
        $group->addPermission('admin-admin-user-update', function (Factory $permission) {
            $permission->label('Admin User Update')
                ->routes('admin.admin-user.edit,admin.admin-user.update');
        });
        $group->addPermission('admin-admin-user-destroy', function (Factory $permission) {
            $permission->label('Admin User Delete')
                ->routes('admin.admin-user.destroy');
        });

        // currency
        // order status
        // page

        // role
        $group = Permission::addGroup('role', function (Group $group) {
            $group->label('Role Permission');
        });
        $group->addPermission('admin-role-list', function (Factory $permission) {
            $permission->label('Role List')
                ->routes('admin.role.index');
        });
        $group->addPermission('admin-role-create', function (Factory $permission) {
            $permission->label('Role Create')
                ->routes('admin.role.create,admin.role.store');
        });
        $group->addPermission('admin-role-update', function (Factory $permission) {
            $permission->label('Role Edit')
                ->routes('admin.role.edit,admin.role.update');
        });
        $group->addPermission('admin-role-destroy', function (Factory $permission) {
            $permission->label('Role Delete')
                ->routes('admin.role.destroy');
        });

        // state
        // property
        // attribute
        // user-group
        // tax group

        // product
        $productGroup = Permission::addGroup('product', function (Group $group) {
            $group->label('Product Permission');
        });
        $productGroup->addPermission('admin-product-index', function (Factory $permission) {
            $permission->label('Product List')
                ->routes('admin.product.index');
        });
        $productGroup->addPermission('admin-product-create', function (Factory $permission) {
            $permission->label('Product Create')
                ->routes('admin.product.create,admin.product.store');
        });
        $productGroup->addPermission('admin-product-edit', function (Factory $permission) {
            $permission->label('Product Edit')
                ->routes('admin.product.edit,admin.product.update');
        });
        $productGroup->addPermission('admin-product-destroy', function (Factory $permission) {
            $permission->label('Product Delete')
                ->routes('admin.product.edit,admin.product.destroy');
        });

        // order group

        // Blade::if('hasPermission', function ($routeName) {
        //     $condition = false;

        //     /** @var \Harmony\Database\Models\AdminUser */
        //     $user = Auth::guard('admin')->user();
        //     if (! $user) {
        //         $condition = $user->hasPermission($routeName) ?: false;
        //     }

        //     $converted_res = ($condition) ? 'true' : 'false';
        // });
    }
}
