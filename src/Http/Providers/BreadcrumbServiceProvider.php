<?php

namespace Harmony\Http\Providers;

use Harmony\Breadcrumbs\Builder;
use Harmony\Breadcrumbs\Factory;
use Harmony\Support\Facades\Breadcrumb;
use Illuminate\Support\ServiceProvider;

class BreadcrumbServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();

        $this->app->alias('breadcrumb', 'Harmony\Breadcrumbs\Builder');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootBreadcrumbs();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['breadcrumb', 'Harmony\Breadcrumbs\Builder'];
    }

    /**
     * Register the provider service.
     *
     * @return void
     */
    protected function registerService()
    {
        $this->app->singleton('breadcrumb', function () {
            return new Builder();
        });
    }

    protected function bootBreadcrumbs()
    {
        Breadcrumb::make('admin.dashboard', function (Factory $breadcrumb) {
            $breadcrumb->label('Dashboard');
        });

        // role
        Breadcrumb::make('admin.role.index', function (Factory $breadcrumb) {
            $breadcrumb->label('Role');
            $breadcrumb->parent('admin.dashboard');
        });
        Breadcrumb::make('admin.role.create', function (Factory $breadcrumb) {
            $breadcrumb->label('Role Create');
            $breadcrumb->parent('admin.dashboard');
            $breadcrumb->parent('admin.role.index');
        });
        Breadcrumb::make('admin.role.edit', function (Factory $breadcrumb) {
            $breadcrumb->label('Role Edit');
            $breadcrumb->parent('admin.dashboard');
            $breadcrumb->parent('admin.role.index');
        });

        // language
        // order-status
        // admin-user
        Breadcrumb::make('admin.admin-user.index', function (Factory $breadcrumb) {
            $breadcrumb->label('Admin User');
            $breadcrumb->parent('admin.dashboard');
        });
        Breadcrumb::make('admin.admin-user.create', function (Factory $breadcrumb) {
            $breadcrumb->label('Admin User Create');
            $breadcrumb->parent('admin.dashboard');
            $breadcrumb->parent('admin.admin-user.index');
        });
        Breadcrumb::make('admin.admin-user.edit', function (Factory $breadcrumb) {
            $breadcrumb->label('Admin User Edit');
            $breadcrumb->parent('admin.dashboard');
            $breadcrumb->parent('admin.admin-user.index');
        });
    }
}
