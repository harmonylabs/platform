<?php

namespace Harmony\Http\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class ResetPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(Lang::get('Reset Password Notification'))
            ->line(Lang::get('Someone, hopefuly you, has requested to reset the password for your account.'))
            ->action(Lang::get('Reset Password'), url(route('password.reset', ['token' => $this->token, 'email' => $this->email], false)))
            ->line(Lang::get('This password reset link will expire in :count minutes', ['count' => config('platform.auth.passwords.adminusers.expire')]))
            ->line(Lang::get('If you did not request a password reset, no further action is required.'));
    }
}
