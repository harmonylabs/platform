<?php

namespace Harmony\Http\Middleware;

use Closure;
use Harmony\Database\Contracts\CurrencyRepository;
use Illuminate\Support\Facades\Session;

class HasDefaultCurrency
{
    /**
     * The currency repository implementation.
     *
     * @var \Harmony\Database\Contracts\CurrencyRepository
     */
    protected $currencyRepository;

    /**
     * Creates a new middleware instance.
     *
     * @param  \Harmony\Database\Contracts\CurrencyRepository  $currencyRepository
     * @return void
     */
    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->setDefaultCurrency();

        return $next($request);
    }

    /**
     * Put a default currency value in the session storage.
     *
     * @return void
     */
    public function setDefaultCurrency()
    {
        if (! Session::has('default_currency')) {
            $currency = $this->currencyRepository->all()->first();

            Session::put('default_currency', $currency);
        }
    }
}
