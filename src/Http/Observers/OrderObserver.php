<?php

namespace Harmony\Http\Observers;

use Harmony\Database\Models\Order;

class OrderObserver
{
    /**
     * Handle the "created" event.
     *
     * @param  \Harmony\Database\Models\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        //
    }
}
