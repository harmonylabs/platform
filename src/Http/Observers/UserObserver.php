<?php

namespace Harmony\Http\Observers;

use Harmony\Database\Contracts\UserGroupRepository;

class UserObserver
{
    /**
     * The user group repository implementation.
     *
     * @var \Harmony\Database\Contracts\UserGroupRepository
     */
    protected $userGroup;

    /**
     * Create a new observer instance.
     *
     * @param  \Harmony\Database\Contracts\UserGroupRepository  $userGroup
     * @return void
     */
    public function __construct(UserGroupRepository $userGroup)
    {
        $this->userGroup = $userGroup;
    }

    /**
     * Handle the "created" event.
     *
     * @param  mixed  $user
     * @return void
     */
    public function created($user)
    {
        $userGroup = $this->userGroup->getIsDefault();

        $user->user_group_id = $userGroup->id;
        $user->save();
    }
}
