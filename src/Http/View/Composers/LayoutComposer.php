<?php

namespace Harmony\Http\View\Composers;

use Harmony\Support\Facades\Menu;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class LayoutComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $routeName = Route::currentRouteName();

        [$openKey, $itemKey] = Menu::getItemFromRouteName($routeName);

        $adminMenus = Menu::adminMenus();

        $view->with('adminMenus', $adminMenus)
            ->with('openKey', $openKey)
            ->with('itemKey', $itemKey);
    }
}
