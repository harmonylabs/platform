<?php

namespace Harmony\Http\Listeners;

use Harmony\Http\Events\OrderProductCreated;

class SaveOrderProduct
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(OrderProductCreated $event)
    {
        $orderProduct = $event->orderProduct;

        $product = $orderProduct->product;
        $product->qty -= $orderProduct->qty;
        $product->save();
    }
}
