<?php

namespace Harmony\Http\Controllers;

use Harmony\Database\Contracts\UserGroupRepository;
use Harmony\Database\Models\UserGroup;
use Harmony\Http\Requests\UserGroupRequest;
use Harmony\Support\Facades\Tabs;

class UserGroupController
{
    /**
     * The user group repository implementation.
     *
     * @var \Harmony\Database\Contracts\UserGroupRepository
     */
    protected $userGroupRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Harmony\Database\Contracts\UserGroupRepository  $userGroupRepository
     * @return void
     */
    public function __construct(UserGroupRepository $userGroupRepository)
    {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $userGroups = $this->userGroupRepository->paginate();

        return view('platform::user.user-group.index')
            ->with(compact('userGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tabs::get('user.user-group');

        return view('platform::user.user-group.create')
            ->with(compact('tabs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Harmony\Http\Requests\UserGroupRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserGroupRequest $request)
    {
        $this->userGroupRepository->create($request->all());

        return redirect()
            ->route('admin.user-group.index')
            ->with('notification', __('User Group Successfully Stored!'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Harmony\Database\Models\UserGroup  $userGroup
     * @return \Illuminate\View\View
     */
    public function edit(UserGroup $userGroup)
    {
        $tabs = Tabs::get('user.user-group');

        return view('platform::user.user-group.edit')
            ->with(compact('userGroup', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Harmony\Http\Requests\UserGroupRequest  $request
     * @param  \Harmony\Database\Models\UserGroup  $userGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserGroupRequest $request, UserGroup $userGroup)
    {
        if ($request->get('is_default')) {
            $group = $this->userGroupRepository->getIsDefault();
            $group->update(['is_default' => 0]);
        }

        $userGroup->update($request->all());

        return redirect()
            ->route('admin.user-group.index')
            ->with('notification', __('User Group Successfully Updated!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Harmony\Database\Models\UserGroup  $userGroup
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(UserGroup $userGroup)
    {
        $userGroup->delete();

        return response()->json([
            'success' => true,
            'message' => __('User Group Successfully Deleted!'),
        ]);
    }
}
