<?php

namespace Harmony\Http\Controllers;

use Harmony\Support\Facades\Widget;

class DashboardController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $orderWidget = Widget::get('platform-total-order');
        $customerWidget = Widget::get('platform-total-customer');
        $revenueWidget = Widget::get('platform-total-revenue');

        return view('platform::admin', compact('orderWidget', 'customerWidget', 'revenueWidget'));
    }
}
