<?php

namespace Harmony\Http\Controllers;

use Harmony\Database\Contracts\ConfigurationRepository;
use Harmony\Support\Facades\Tabs;
use Illuminate\Http\Request;

class ConfigurationController
{
    /**
     * The configuration repository instance
     *
     * @var \Harmony\Database\Contracts\ConfigurationRepository
     */
    protected $configurationRepository;

    /**
     * Create a new controller instance
     *
     * @param  \Harmony\Database\Contracts\ConfigurationRepository $configurationRepository
     * @return void
     */
    public function __construct(ConfigurationRepository $configurationRepository)
    {
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $tabs = Tabs::get('system.configuration');

        return view('platform::system.configuration.index')
            ->with('tabs', $tabs)
            ->with('repository', $this->configurationRepository);
    }

    /**
     * Store a newly created resource in storage
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        foreach ($request->except('_token') as $code => $value) {
            $model = $this->configurationRepository->getModelByCode($code);
            if ($model === null) {
                $this->configurationRepository->create([
                    'code' => $code,
                    'value' => $value
                ]);
            } else {
                $model->update(['value' => $value]);
            }
        }

        return redirect()
            ->route('admin.configuration.index')
            ->with('notification', __('Configuration Successfully Saved!'));
    }
}
