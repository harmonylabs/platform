<?php

namespace Harmony\Http\Controllers;

use Harmony\Database\Contracts\PermissionRepository;
use Harmony\Database\Contracts\RoleRepository;
use Harmony\Database\Models\Role;
use Harmony\Http\Requests\RoleRequest;
use Harmony\Support\Facades\Permission;
use Harmony\Support\Facades\Tabs;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;

class RoleController extends Controller
{
    /**
     * The role repository instance.
     *
     * @var \Harmony\Database\Contracts\RoleRepository
     */
    protected $roleRepository;

    /**
     * The permission repository instance.
     *
     * @var \Harmony\Database\Contracts\PermissionRepository
     */
    protected $permissionRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Harmony\Database\Contracts\RoleRepository  $roleRepository
     * @param  \Harmony\Database\Contracts\PermissionRepository  $permissionRepository
     * @return void
     */
    public function __construct(RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $roles = $this->roleRepository->paginate();

        return view('platform::system.role.index')
            ->with(compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tabs::get('role');
        $permissions = Permission::all();

        return view('platform::system.role.create')
            ->with(compact('permissions', 'tabs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Harmony\Http\Requests\RoleRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RoleRequest $request)
    {
        $role = $this->roleRepository->create($request->all());
        $this->saveRolePermissions($request, $role);

        return redirect()
            ->route('admin.role.index')
            ->with('notification', __('Role Successfully Stored!'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Harmony\Database\Models\Role  $role
     * @return \Illuminate\View\View
     */
    public function edit(Role $role)
    {
        $tabs = Tabs::get('system.role');
        $permissions = Permission::all();

        return view('platform::system.role.edit')
            ->with(compact('role', 'permissions', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Harmony\Http\Requests\RoleRequest  $request
     * @param  \Harmony\Database\Models\Role  $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RoleRequest $request, Role $role)
    {
        $role->update($request->all());
        $this->saveRolePermissions($request, $role);

        return redirect()
            ->route('admin.role.index')
            ->with('notification', __('Role Successfully Updated!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Harmony\Database\Models\Role  $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Role $role)
    {
        $role->delete();

        return response()->json([
            'success' => true,
            'message' => __('Role Successfully Deleted!'),
        ]);
    }
}
