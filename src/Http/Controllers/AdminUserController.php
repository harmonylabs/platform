<?php

namespace Harmony\Http\Controllers;

use Harmony\Database\Contracts\AdminUserRepository;
use Harmony\Database\Contracts\RoleRepository;
use Harmony\Database\Models\AdminUser;
use Harmony\Http\Requests\AdminUserImageRequest;
use Harmony\Http\Requests\AdminUserRequest;
use Harmony\Support\Facades\Tabs;
use Illuminate\Routing\Controller;

class AdminUserController extends Controller
{
    /**
     * The admin user repository implementation.
     *
     * @var \Harmony\Database\Contracts\AdminUserRepository
     */
    protected $adminUserRepository;

    /**
     * The role repository implementation.
     *
     * @var \Harmony\Database\Contracts\RoleRepository
     */
    protected $roleRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Harmony\Database\Contracts\AdminUserRepository  $adminUserRepository
     * @param  \Harmony\Database\Contracts\RoleRepository  $roleRepository
     * @return void
     */
    public function __construct(AdminUserRepository $adminUserRepository, RoleRepository $roleRepository)
    {
        $this->adminUserRepository = $adminUserRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $adminUsers = $this->adminUserRepository->paginate();

        return view('platform::user.admin-user.index')
            ->with(compact('adminUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabs = Tabs::get('user.admin-user');
        $languageOptions = ['en' => 'English'];
        $roleOptions = $this->roleRepository->options();

        return view('platform::user.admin-user.create')
            ->with(compact('roleOptions', 'tabs', 'languageOptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Harmony\Http\Requests\AdminUserRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdminUserRequest $request)
    {
        $request->merge([
            'password' => bcrypt($request->password),
        ]);

        $this->adminUserRepository->create($request->all());

        return redirect()
            ->route('admin.admin-user.index')
            ->with('notification', __('Admin User Successfully Stored!'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Harmony\Database\Models\AdminUser  $adminUser
     * @return \Illuminate\View\View
     */
    public function edit(AdminUser $adminUser)
    {
        $tabs = Tabs::get('user.admin-user');
        $languageOptions = ['en' => 'English'];
        $roleOptions = $this->roleRepository->options();

        return view('platform::user.admin-user.edit')
            ->with(compact('adminUser', 'roleOptions', 'tabs', 'languageOptions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Harmony\Http\Requests\AdminUserRequest  $request
     * @param  \Harmony\Database\Models\AdminUser  $adminUser
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminUserRequest $request, AdminUser $adminUser)
    {
        $adminUser->update($request->all());

        return redirect()
            ->route('admin.admin-user.index')
            ->with('notification', __('Admin User Successfully Updated!'));
    }

    /**
     * Remove the specified resource from the storage.
     *
     * @param  \Harmony\Database\Models\AdminUser  $adminUser
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(AdminUser $adminUser)
    {
        $adminUser->delete();

        return response()->json([
            'success' => true,
            'message' => __('Admin User Successfully Deleted!'),
        ]);
    }

    /**
     * Upload an image to the storage.
     *
     * @param  \Harmony\Http\Requests\AdminUserImageRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(AdminUserImageRequest $request)
    {
        $image = $request->file('files');
        $path = $image->store('uploads/users', 'platform');

        return response()->json([
            'success' => true,
            'path' => $path,
            'message' => __('Admin User Image Successfully Uploaded!'),
        ]);
    }
}
