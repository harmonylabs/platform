<?php

namespace Harmony\Tabs;

class Factory
{
    /**
     * The specified tab label.
     *
     * @var string
     */
    protected $label;

    /**
     * The specified tab view path.
     *
     * @var string
     */
    protected $view;

    /**
     * The specified tab key.
     *
     * @var string
     */
    protected $key;

    /**
     * Create a new factory instance.
     *
     * @param  callable  $callable
     * @return void
     */
    public function __construct(callable $callable)
    {
        $callable($this);
    }

    /**
     * Get / set the specified tab label.
     *
     * @param  string|null  $label
     * @return mixed
     */
    public function label($label = null)
    {
        if (null !== $label) {
            $this->label = $label;

            return $this;
        }

        return trans($this->label);
    }

    /**
     * Get / set the specified tab view path.
     *
     * @param  string|null  $view
     * @return mixed
     */
    public function view($view = null)
    {
        if (null !== $view) {
            $this->view = $view;

            return $this;
        }

        return $this->view;
    }

    /**
     * Get / set the specified tab key.
     *
     * @param  string|null  $key
     * @return mixed
     */
    public function key($key = null)
    {
        if (null !== $key) {
            $this->key = $key;

            return $this;
        }

        return $this->key;
    }
}
