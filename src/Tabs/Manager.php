<?php

namespace Harmony\Tabs;

use Illuminate\Support\Collection;

class Manager
{
    /**
     * The collection instance.
     *
     * @var \Illuminate\Support\Collection
     */
    public $collection;

    /**
     * Create a new manager instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->collection = Collection::make([]);
    }

    /**
     * Get an item from the collection by key.
     *
     * @param  string  $key
     * @return \Illuminate\Support\Collection
     */
    public function get(string $key): Collection
    {
        return $this->collection->get($key);
    }

    /**
     * Return all items from the collection.
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(): Collection
    {
        return $this->collection;
    }

    /**
     * Put an item to the collection by key.
     *
     * @param  string  $key
     * @param  callable  $tab
     * @return $this
     */
    public function put(string $key, callable $tab)
    {
        $obj = new Factory($tab);

        if (! $this->collection->has($key)) {
            $collection = Collection::make([]);
            $collection->push($obj);
        } else {
            $collection = $this->collection->get($key);
            $collection->push($obj);
        }

        $this->collection->put($key, $collection);

        return $this;
    }
}
