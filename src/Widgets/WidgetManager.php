<?php

namespace Harmony\Widgets;

use Illuminate\Support\Collection;

class WidgetManager
{
    /**
     * The widget collection instance.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $widgets;

    /**
     * Create a new manager instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->widgets = Collection::make([]);
    }

    /**
     * Create a widget instance in the collection by name.
     *
     * @param  string  $key
     * @param  \Harmony\Widgets\Factory  $factory
     * @return \Harmony\Widgets\Factory
     */
    public function make($key, $factory)
    {
        $this->widgets->put($key, $factory);

        return $factory;
    }

    /**
     * Get a widget item from the collection by key.
     *
     * @param  string  $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->widgets->get($key);
    }

    /**
     * Get all of the widget items in the collection.
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        return $this->widgets;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function options(): Collection
    {
        $options = Collection::make([]);

        foreach ($this->widgets as $key => $factory) {
            $options->put($key, $factory->label());
        }

        return $options;
    }
}
