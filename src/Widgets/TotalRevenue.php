<?php

namespace Harmony\Widgets;

use Harmony\Database\Contracts\OrderRepository;

class TotalRevenue
{
    /**
     * The widgets specified view path.
     *
     * @var string
     */
    protected $view = 'platform::widget.total-revenue';

    /**
     * The widgets specified label.
     *
     * @var string
     */
    protected $label = 'Total Revenue';

    /**
     * The widgets specified type.
     *
     * @var string
     */
    protected $type = 'cms';

    /**
     * The widgets specified identifier.
     *
     * @var string
     */
    protected $identifier = 'platform-total-revenue';

    /**
     * Get the specified view path of the widget.
     *
     * @return string
     */
    public function view()
    {
        return $this->view;
    }

    /**
     * Get the specified label of the widget.
     *
     * @return string
     */
    public function label()
    {
        return $this->label;
    }

    /**
     * Get the specified type of the widget.
     *
     * @return string
     */
    public function type()
    {
        return $this->type;
    }

    /**
     * Get the specified identifier of the widget.
     *
     * @return string
     */
    public function identifier()
    {
        return $this->identifier;
    }

    /**
     * Add a piece of data to the view.
     *
     * @return mixed
     */
    public function with()
    {
        $orderRepository = app(OrderRepository::class);
        $value = $orderRepository->getCurrentMonthTotalRevenue();

        return ['value' => $value];
    }

    /**
     * Render the string contents of the view.
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        return view($this->view(), $this->with());
    }
}
