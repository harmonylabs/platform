<?php

namespace Harmony\Widgets;

use Illuminate\Support\Carbon;

class TotalCustomer
{
    /**
     * The widgets specified view path.
     *
     * @var string
     */
    protected $view = "platform::widget.total-customer";

    /**
     * The widgets specified label.
     *
     * @var string
     */
    protected $label = 'Total Customer';

    /**
     * The widgets specified type.
     *
     * @var string
     */
    protected $type = "cms";

    /**
     * The widgets specified identifier.
     *
     * @var string
     */
    protected $identifier = "platform-total-customer";

    /**
     * Get the specified view path of the widget.
     *
     * @return string
     */
    public function view()
    {
        return $this->view;
    }

    /**
     * Get the specified label of the widget.
     *
     * @return string
     */
    public function label()
    {
        return $this->label;
    }

    /**
     * Get the specified type of the widget.
     *
     * @return string
     */
    public function type()
    {
        return $this->type;
    }

    /**
     * Get the specified identifier of the widget.
     *
     * @return string
     */
    public function identifier()
    {
        return $this->identifier;
    }

    /**
     * Add a piece of data to the view.
     *
     * @return mixed
     */
    public function with()
    {
        $userModel = $this->getUserModel();
        $firstDay = $this->getFirstDay();

        if ($userModel === null) {
            $value = 0;
        } else {
            $value = $userModel->select('id')->where('created_at', '>', $firstDay)->count();
        }

        return ['value' => $value];
    }

    /**
     * Render the string contents of the view.
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        return view($this->view(), $this->with());
    }

    /**
     * Get the starting day of the month.
     *
     * @return mixed
     */
    protected function getFirstDay()
    {
        $startDay = Carbon::now();

        return $startDay->firstOfMonth();
    }

    /**
     * Get the user model.
     *
     * @return mixed
     */
    protected function getUserModel()
    {
        $user = config('platform.user.model');

        try {
            $model = app($user);
        } catch (\Exception $e) {
            $model = null;
        }

        return $model;
    }
}
