<?php

namespace Harmony\Widgets;

use Harmony\Contracts\Widget;

class Factory implements Widget
{
    /**
     * The specified widget label.
     *
     * @var string|null
     */
    public $label = null;

    /**
     * The specified widget type.
     *
     * @var string|null
     */
    public $type = null;

    /**
     * The callback instance.
     *
     * @var callable|null
     */
    protected $callable = null;

    /**
     * Create a new widget factory instance.
     *
     * @param  callable  $callable
     * @return void
     */
    public function __construct($callable)
    {
        $this->callable = $callable;
        $callable($this);
    }

    /**
     * {@inheritDoc}
     */
    public function label($label = null)
    {
        if (null === $label) {
            return $this->label;
        }

        $this->label = $label;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function type($type = null)
    {
        if (null === $type) {
            return $this->type;
        }

        $this->type = $type;

        return $this;
    }
}
