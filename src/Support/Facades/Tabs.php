<?php

namespace Harmony\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \Illuminate\Support\Collection get(string $key)
 * @method static \Illuminate\Support\Collection all()
 * @method static \Harmony\Tabs\Manager put(string $key, callable $tab)
 *
 * @see \Harmony\Tabs\Manager
 */
class Tabs extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'tabs';
    }
}
