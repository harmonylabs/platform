<?php

namespace Harmony\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \Illuminate\Support\Collection get(string $key)
 * @method static \Illuminate\Support\Collection all()
 * @method static \Harmony\Permission\Manager set(string $key, mixed $permission)
 * @method static \Harmony\Permission\Group addGroup(string $key, $callable)
 */
class Permission extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'permission';
    }
}
