<?php

namespace Harmony\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed get($key)
 * @method static \Harmony\Breadcrumbs\Builder make($name, callable $callable)
 * @method static string|\Illuminate\View\View render($routeName)
 *
 * @see \Harmony\Breadcrumbs\Builder
 */
class Breadcrumb extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'breadcrumb';
    }
}
