<?php

namespace Harmony\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \Harmony\Widgets\Factory make($key, $factory)
 * @method static mixed get($key)
 * @method static \Illuminate\Support\Collection all()
 * @method static \Illuminate\Support\Collection options()
 */
class Widget extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'widget';
    }
}
