<?php

namespace Harmony\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \Harmony\Menu\Builder make($key, callable $callable)
 * @method static mixed get($key)
 * @method static \Illuminate\Support\Collection all($admin = false)
 * @method static \Illuminate\Support\Collection frontMenus()
 * @method static \Illuminate\Support\Collection adminMenus()
 * @method static array getItemFromRouteName($name)
 */
class Menu extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'menu';
    }
}
