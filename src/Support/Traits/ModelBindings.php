<?php

namespace Harmony\Support\Traits;

use Harmony\Database\Contracts\AddressRepository as AddressRepositoryContract;
use Harmony\Database\Contracts\AdminUserRepository as AdminUserRepositoryContract;
use Harmony\Database\Contracts\AttributeRepository as AttributeRepositoryContract;
use Harmony\Database\Contracts\CategoryFilterRepository as CategoryFilterRepositoryContract;
use Harmony\Database\Contracts\CategoryRepository as CategoryRepositoryContract;
use Harmony\Database\Contracts\ConfigurationRepository as ConfigurationRepositoryContract;
use Harmony\Database\Contracts\CountryRepository as CountryRepositoryContract;
use Harmony\Database\Contracts\CurrencyRepository as CurrencyRepositoryContract;
use Harmony\Database\Contracts\CustomerRepository as CustomerRepositoryContract;
use Harmony\Database\Contracts\LanguageRepository as LanguageRepositoryContract;
use Harmony\Database\Contracts\MenuGroupRepository as MenuGroupRepositoryContract;
use Harmony\Database\Contracts\MenuRepository as MenuRepositoryContract;
use Harmony\Database\Contracts\OrderProductRepository as OrderProductRepositoryContract;
use Harmony\Database\Contracts\OrderRepository as OrderRepositoryContract;
use Harmony\Database\Contracts\OrderStatusRepository as OrderStatusRepositoryContract;
use Harmony\Database\Contracts\PermissionRepository as PermissionRepositoryContract;
use Harmony\Database\Contracts\ProductImageRepository as ProductImageRepositoryContract;
use Harmony\Database\Contracts\ProductRepository as ProductRepositoryContract;
use Harmony\Database\Contracts\RoleRepository as RoleRepositoryContract;
use Harmony\Database\Contracts\StateRepository as StateRepositoryContract;
use Harmony\Database\Contracts\TaxGroupRepository as TaxGroupRepositoryContract;
use Harmony\Database\Contracts\TaxRateRepository as TaxRateRepositoryContract;
use Harmony\Database\Contracts\UserGroupRepository as UserGroupRepositoryContract;
use Harmony\Database\Repositories\AddressRepository;
use Harmony\Database\Repositories\AdminUserRepository;
use Harmony\Database\Repositories\AttributeRepository;
use Harmony\Database\Repositories\CategoryFilterRepository;
use Harmony\Database\Repositories\CategoryRepository;
use Harmony\Database\Repositories\ConfigurationRepository;
use Harmony\Database\Repositories\CountryRepository;
use Harmony\Database\Repositories\CurrencyRepository;
use Harmony\Database\Repositories\CustomerRepository;
use Harmony\Database\Repositories\LanguageRepository;
use Harmony\Database\Repositories\MenuGroupRepository;
use Harmony\Database\Repositories\MenuRepository;
use Harmony\Database\Repositories\OrderProductRepository;
use Harmony\Database\Repositories\OrderRepository;
use Harmony\Database\Repositories\OrderStatusRepository;
use Harmony\Database\Repositories\PermissionRepository;
use Harmony\Database\Repositories\ProductImageRepository;
use Harmony\Database\Repositories\ProductRepository;
use Harmony\Database\Repositories\RoleRepository;
use Harmony\Database\Repositories\StateRepository;
use Harmony\Database\Repositories\TaxGroupRepository;
use Harmony\Database\Repositories\TaxRateRepository;
use Harmony\Database\Repositories\UserGroupRepository;

trait ModelBindings
{
    /**
     * The repository model bindings.
     *
     * @var array
     */
    public $models = [
        AddressRepositoryContract::class => AddressRepository::class,
        AdminUserRepositoryContract::class => AdminUserRepository::class,
        AttributeRepositoryContract::class => AttributeRepository::class,
        CategoryFilterRepositoryContract::class => CategoryFilterRepository::class,
        CategoryRepositoryContract::class => CategoryRepository::class,
        CountryRepositoryContract::class => CountryRepository::class,
        ConfigurationRepositoryContract::class => ConfigurationRepository::class,
        CurrencyRepositoryContract::class => CurrencyRepository::class,
        CustomerRepositoryContract::class => CustomerRepository::class,
        LanguageRepositoryContract::class => LanguageRepository::class,
        MenuGroupRepositoryContract::class => MenuGroupRepository::class,
        MenuRepositoryContract::class => MenuRepository::class,
        OrderProductRepositoryContract::class => OrderProductRepository::class,
        OrderRepositoryContract::class => OrderRepository::class,
        OrderStatusRepositoryContract::class => OrderStatusRepository::class,
        PermissionRepositoryContract::class => PermissionRepository::class,
        ProductImageRepositoryContract::class => ProductImageRepository::class,
        ProductRepositoryContract::class => ProductRepository::class,
        RoleRepositoryContract::class => RoleRepository::class,
        StateRepositoryContract::class => StateRepository::class,
        TaxGroupRepositoryContract::class => TaxGroupRepository::class,
        TaxRateRepositoryContract::class => TaxRateRepository::class,
        UserGroupRepositoryContract::class => UserGroupRepository::class,
    ];
}
