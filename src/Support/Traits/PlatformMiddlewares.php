<?php

namespace Harmony\Support\Traits;

use Harmony\Http\Middleware\AdminAuthenticate;
use Harmony\Http\Middleware\CheckPermissions;
use Harmony\Http\Middleware\HasDefaultCurrency;
use Harmony\Http\Middleware\RedirectIfAdminAuthenticated;

trait PlatformMiddlewares
{
    /**
     * The middleware mappings of the package.
     *
     * @var array
     */
    protected $middlewares = [
        'admin.auth' => AdminAuthenticate::class,
        'permission' => CheckPermissions::class,
        'admin.guest' => RedirectIfAdminAuthenticated::class,
        'platform' => HasDefaultCurrency::class,
    ];
}
