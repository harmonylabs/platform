<?php

namespace Harmony\Support\Traits;

trait PlatformProviders
{
    /**
     * The provider mappings of the package.
     *
     * @var array
     */
    protected $providers = [
        \Harmony\Http\Providers\BreadcrumbServiceProvider::class,
        \Harmony\Http\Providers\EventServiceProvider::class,
        \Harmony\Http\Providers\MenuServiceProvider::class,
        \Harmony\Http\Providers\PermissionServiceProvider::class,
        \Harmony\Http\Providers\RepositoryServiceProvider::class,
        \Harmony\Http\Providers\TabsServiceProvider::class,
        \Harmony\Http\Providers\WidgetServiceProvider::class,
    ];
}
