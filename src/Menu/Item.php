<?php

namespace Harmony\Menu;

use Harmony\Contracts\Menu;

class Item implements Menu
{
    const FRONT = 'front';

    const ADMIN = 'admin';

    /**
     * The menu item key.
     *
     * @var string
     */
    public $key;

    /**
     * The menu item label.
     *
     * @var string
     */
    public $label;

    /**
     * The menu item icon.
     *
     * @var string
     */
    public $icon;

    /**
     * The menu item attributes.
     *
     * @var array
     */
    public $attributes;

    /**
     * The menu item route name.
     *
     * @var string
     */
    public $routeName;

    /**
     * The menu item type.
     *
     * @var string
     */
    public $type;

    /**
     * The menu item parameters.
     *
     * @var string
     */
    public $parameters;

    /**
     * The sub-menu property.
     *
     * @var mixed
     */
    public $subMenu;

    /**
     * The callable instance.
     *
     * @var callable
     */
    public $callable;

    /**
     * Create a new menu item instance.
     *
     * @param  callable  $callable
     * @return void
     */
    public function __construct($callable)
    {
        $this->callable = $callable;
        $callable($this);
    }

    /**
     * {@inheritDoc}
     */
    public function key($key = null)
    {
        if (null !== $key) {
            $this->key = $key;

            return $this;
        }

        return $this->key;
    }

    /**
     * {@inheritDoc}
     */
    public function label($label = null)
    {
        if (null !== $label) {
            $this->label = $label;

            return $this;
        }

        return trans($this->label);
    }

    /**
     * {@inheritDoc}
     */
    public function icon($icon = null)
    {
        if (null !== $icon) {
            $this->icon = $icon;

            return $this;
        }

        return $this->icon;
    }

    /**
     * {@inheritDoc}
     */
    public function attributes($attributes = null)
    {
        if (null !== $attributes) {
            $this->attributes = $attributes;

            return $this;
        }

        return $this->attributes;
    }

    /**
     * {@inheritDoc}
     */
    public function route($routeName = null)
    {
        if (null !== $routeName) {
            $this->routeName = $routeName;

            return $this;
        }

        return $this->routeName;
    }

    /**
     * Get / set the specified menu item type.
     *
     * @param  string|null  $parameters
     * @return mixed
     */
    public function type($type = null)
    {
        if (null !== $type) {
            $this->type = $type;

            return $this;
        }

        return $this->type;
    }

    /**
     * Get / set the specified menu item parameters.
     *
     * @param  string|null  $parameters
     * @return mixed
     */
    public function parameters($parameters = null)
    {
        if (null !== $parameters) {
            $this->parameters = $parameters;

            return $this;
        }

        return $this->parameters;
    }

    /**
     * Get / set the specified sub-menu.
     *
     * @param  string  $key
     * @param  mixed  $item
     * @return \Harmony\Menu\Item
     */
    public function subMenu($key = null, $item = null)
    {
        if (null === $item) {
            return $this->subMenu;
        }

        $menu = new self($item);
        $this->subMenu[$key] = $menu;

        return $this;
    }

    /**
     * Check if a sub-menu is present in the menu.
     *
     * @return bool
     */
    public function hasSubMenu()
    {
        if (isset($this->subMenu) && count($this->subMenu) > 0) {
            return true;
        }

        return false;
    }
}
