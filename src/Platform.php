<?php

namespace Harmony;

class Platform
{
    /**
     * Get the current platform version
     *
     * @return string
     */
    public static function version()
    {
        return '1.0.0';
    }
}
