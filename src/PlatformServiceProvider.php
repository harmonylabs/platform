<?php

namespace Harmony;

use Harmony\Console\Commands\AdminCommand;
use Harmony\Console\Commands\InstallCommand;
use Harmony\Http\View\Composers\LayoutComposer;
use Harmony\Support\Traits\PlatformMiddlewares;
use Harmony\Support\Traits\PlatformProviders;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class PlatformServiceProvider extends ServiceProvider
{
    use PlatformProviders, PlatformMiddlewares;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerProviders();
        $this->registerConfigs();
        $this->registerRoutes();
        $this->registerMiddlewares();
        $this->registerViewComposer();
        $this->registerCommands();
        $this->registerMigrations();
        $this->registerViews();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootPublishing();
    }

    /**
     * Register the package providers.
     *
     * @return void
     */
    protected function registerProviders()
    {
        foreach ($this->providers as $provider) {
            App::register($provider);
        }
    }

    /**
     * Configure the package configurations.
     *
     * @return void
     */
    protected function registerConfigs()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/platform.php', 'platform');

        $config = $this->app['config'];

        $platform = include __DIR__.'/../config/platform.php';
        $filesystems = $config->get('filesystems', []);
        $auth = $config->get('auth', []);

        $config->set('filesystems', array_merge($filesystems, $platform['filesystems']));
        $config->set('auth.guards', array_merge($auth['guards'], $platform['auth']['guards']));
        $config->set('auth.providers', array_merge($auth['providers'], $platform['auth']['providers']));
        $config->set('auth.passwords', array_merge($auth['passwords'], $platform['auth']['passwords']));
    }

    /**
     * Register the package routes.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
    }

    /**
     * Register the package middlewares.
     *
     * @return void
     */
    protected function registerMiddlewares()
    {
        foreach ($this->middlewares as $alias => $middleware) {
            $this->app['router']->aliasMiddleware($alias, $middleware);
        }
    }

    /**
     * Register the package view composer.
     *
     * @return void
     */
    protected function registerViewComposer()
    {
        View::composer('platform::layouts.app', LayoutComposer::class);
    }

    /**
     * Register the package console commands.
     *
     * @return void
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
                AdminCommand::class,
            ]);
        }
    }

    /**
     * Register the package migrations.
     *
     * @return void
     */
    protected function registerMigrations()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    /**
     * Register the package views.
     *
     * @return void
     */
    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'platform');
    }

    /**
     * Configure publishing for the package.
     *
     * @return void
     */
    protected function bootPublishing()
    {
        if (! $this->app->runningInConsole()) {
            return;
        }

        $this->publishes([
            __DIR__.'/../config/platform.php' => config_path('platform.php'),
        ], 'platform-config');

        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/platform'),
        ], 'platform-assets');
    }
}
