<?php

namespace Harmony\Permission;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Lang;

class Group
{
    /**
     * The specified group label.
     *
     * @var string
     */
    protected $label;

    /**
     * The specified group key.
     *
     * @var string
     */
    protected $key;

    /**
     * The collection group instance.
     *
     * @var \Illuminate\Support\Collection
     */
    public $permissions;

    /**
     * Create a new permission group instance.
     *
     * @param  callable|null  $callable
     * @return void
     */
    public function __construct($callable = null)
    {
        $this->permissions = Collection::make([]);

        if (null !== $callable) {
            $callable($this);
        }
    }

    /**
     * Get / set the specified group label.
     *
     * @param  string|null  $label
     * @return mixed
     */
    public function label($label = null)
    {
        if (null !== $label) {
            $this->label = $label;

            return $this;
        }

        if (Lang::has($this->label)) {
            return __($this->label);
        }

        return $this->label;
    }

    /**
     * Get / set the specified group key.
     *
     * @param  string|null  $key
     * @return mixed
     */
    public function key($key = null)
    {
        if (null !== $key) {
            $this->key = $key;

            return $this;
        }

        return $this->key;
    }

    /**
     * Add a permission to a specified group.
     *
     * @param  string  $key
     * @param  callable|null  $callable
     * @return \Harmony\Permission\Factory
     */
    public function addPermission($key, $callable = null)
    {
        if (null !== $callable) {
            $permission = new Factory($callable);
            $permission->key($key);

            $this->permissions->put($key, $permission);
        } else {
            $permission = new Factory();
            $permission->key($key);

            $this->permissions->put($key, $permission);
        }

        return $permission;
    }
}
