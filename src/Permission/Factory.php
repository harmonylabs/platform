<?php

namespace Harmony\Permission;

use Harmony\Contracts\Permission;
use Illuminate\Support\Facades\Lang;

class Factory implements Permission
{
    /**
     * The specified permission label.
     *
     * @var string
     */
    protected $label;

    /**
     * The specified permission routes.
     *
     * @var string
     */
    protected $routes;

    /**
     * The specified permission key.
     *
     * @var string
     */
    protected $key;

    /**
     * Create a new factory instance.
     *
     * @param  callable|null  $callable
     * @return void
     */
    public function __construct($callable = null)
    {
        if (null !== $callable) {
            $callable($this);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function label($label = null)
    {
        if (null !== $label) {
            $this->label = $label;

            return $this;
        }

        if (Lang::has($this->label)) {
            return __($this->label);
        }

        return $this->label;
    }

    /**
     * {@inheritDoc}
     */
    public function routes($routes = null)
    {
        if (null !== $routes) {
            $this->routes = $routes;

            return $this;
        }

        return $this->routes;
    }

    /**
     * {@inheritDoc}
     */
    public function key($key = null)
    {
        if (null !== $key) {
            $this->key = $key;

            return $this;
        }

        return $this->key;
    }
}
