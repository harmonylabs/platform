<?php

namespace Harmony\Permission;

use Illuminate\Support\Collection;

class Manager
{
    /**
     * The permission collection instance.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $permissions;

    /**
     * Create a new manager instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->permissions = Collection::make([]);
    }

    /**
     * Get an item from the permission collection by key.
     *
     * @param  string  $key
     * @return \Illuminate\Support\Collection
     */
    public function get($key)
    {
        if ($this->permissions->has($key)) {
            return $this->permissions->get($key);
        }

        return Collection::make([]);
    }

    /**
     * Set an item of permission in the collection.
     *
     * @param  string  $key
     * @param  mixed  $permission
     * @return \Harmony\Permission\Manager
     */
    public function set($key, $permission)
    {
        $this->permissions->put($key, $permission);

        return $this;
    }

    /**
     * Return all items from the permission collection.
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        return $this->permissions;
    }

    /**
     * Add a group to a permission collection.
     *
     * @param  string  $key
     * @param  callable|null  $callable
     * @return \Harmony\Permission\Group
     */
    public function addGroup($key, $callable = null)
    {
        if (null !== $callable) {
            $group = new Group($callable);
            $group->key($key);

            $this->permissions->put($key, $group);
        } else {
            $group = new Group();
            $group->key($key);

            $this->permissions->put($key, $group);
        }

        return $group;
    }
}
