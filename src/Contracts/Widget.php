<?php

namespace Harmony\Contracts;

interface Widget
{
    /**
     * Get / set the specified widget label.
     *
     * @return string
     */
    public function label();

    /**
     * Get / set the specified widget type.
     *
     * @return string
     */
    public function type();
}
