<?php

namespace Harmony\Contracts;

interface Permission
{
    /**
     * Get / set the specified permit key.
     *
     * @param  string|null  $routes
     * @return mixed
     */
    public function key($key = null);

    /**
     * Get / set the specified permit label.
     *
     * @param  string|null  $routes
     * @return mixed
     */
    public function label($label = null);

    /**
     * Get / set the specified permit routes.
     *
     * @param  string|null  $routes
     * @return mixed
     */
    public function routes($routes = null);
}
