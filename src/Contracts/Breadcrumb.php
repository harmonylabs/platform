<?php

namespace Harmony\Contracts;

interface Breadcrumb
{
    /**
     * Get / set the specified breadcrumb label.
     *
     * @return string
     */
    public function label();

    /**
     * Get / set the specified breadcrumb route.
     *
     * @return string
     */
    public function route();
}
