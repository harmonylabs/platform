<?php

namespace Harmony\Contracts;

interface Menu
{
    /**
     * Get / set the specified menu key.
     *
     * @param  string|null  $key
     * @return mixed
     */
    public function key($key = null);

    /**
     * Get / set the specified menu label.
     *
     * @param  string|null  $label
     * @return mixed
     */
    public function label($label = null);

    /**
     * Get / set the specified menu icon.
     *
     * @param  string|null  $icon
     * @return mixed
     */
    public function icon($icon = null);

    /**
     * Get / set the specified menu attributes.
     *
     * @param  array  $attributes
     * @return mixed
     */
    public function attributes($attributes = []);

    /**
     * Get / set the specified menu route name.
     *
     * @param  string|null  $routeName
     * @return mixed
     */
    public function route($routeName = null);
}
