<?php

namespace Harmony\Console\Commands;

use Harmony\Database\Contracts\AdminUserRepository;
use Harmony\Database\Contracts\RoleRepository;
use Illuminate\Console\Command;

class AdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'harmony:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Harmony Platform Admin User';

    /**
     * The role repository instance.
     *
     * @var \Harmony\Database\Contracts\RoleRepository
     */
    protected $roleRepository;

    /**
     * The admin user repository instance.
     *
     * @var \Harmony\Database\Contracts\AdminUserRepository
     */
    protected $adminUserRepository;

    /**
     * Create a new command instance.
     *
     * @param  \Harmony\Database\Contracts\RoleRepository       $roleRepository
     * @param  \Harmony\Database\Contracts\AdminUserRepository  $adminUserRepository
     * @return void
     */
    public function __construct(RoleRepository $roleRepository, AdminUserRepository $adminUserRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->adminUserRepository = $adminUserRepository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Create a Harmony Platform admin user');

        $data['first_name'] = $this->ask('Enter first name');
        $data['last_name'] = $this->ask('Enter last name');
        $data['email'] = $this->ask('Enter email address');
        $data['password'] = $this->secret('Enter password');
        $data['confirm_password'] = $this->secret('Confirm your password');

        // check if password match
        // if ($data['password'] !== $data['confirm_password']) {
        //     $this->error('The password you entered do not match!');
        // }

        $role = $this->roleRepository->findAdminRole();

        $data['role_id'] = $role->id;
        $data['is_super_admin'] = 1;
        $data['password'] = bcrypt($data['password']);

        $this->adminUserRepository->create($data);

        $this->info('Successfully created admin user');
    }

    protected function installDummyData()
    {
        if ($this->confirm('Do you want to install sample data to start with?')) {
            // $this->call('cartel:module', ['install']);
            $this->info('This is still in development.');
        }
    }
}
