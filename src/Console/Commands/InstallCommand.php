<?php

namespace Harmony\Console\Commands;

use Harmony\Database\Contracts\RoleRepository;
use Harmony\Database\Models\Role;
use Harmony\PlatformServiceProvider;
use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'harmony:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the Harmony Platform resources';

    /**
     * The progress bar instance.
     *
     * @var \Symfony\Component\Console\Helper\ProgressBar
     */
    protected $progressBar;

    protected $role;

    protected $orderStatus;

    protected $userGroup;

    protected $currency;

    protected $language;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RoleRepository $role)
    {
        $this->role = $role;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->progressBar = $this->output->createProgressBar(6);
        $this->startSetup();
        sleep(1);

        if (! $this->progressBar->getProgress()) {
            $this->progressBar->start();
        }

        $this->output->writeln('');
        $this->call('vendor:publish', ['--provider' => PlatformServiceProvider::class]);
        $this->progressBar->advance();

        $this->runDatabase();
        $this->call('storage:link');
        $this->progressBar->advance();

        // ask if demo data should be installed

        // ask if admin account should be created
        $this->line('');
        if ($this->confirm('Create an admin account now?')) {
            $this->call('harmony:admin');
            $this->line('');
        }

        $this->line('');
        $this->endSetup();
    }

    /**
     * Run the database migration and seeder.
     *
     * @return void
     */
    protected function runDatabase()
    {
        $this->line('');
        $this->info('Running database migrations...');
        $this->call('migrate:fresh');
        $this->progressBar->advance();

        $this->role->create(['name' => Role::ADMIN]);

        $this->line('');
        $this->info('Running database seeder...');
        $this->call('db:seed', ['--class' => 'DatabaseSeeder']);
        $this->progressBar->advance();

        $this->alterUsersTable();

        // visually slow down the installation process so that the user can read whats happening
        usleep(350000);
    }

    /**
     * Display the end setup script.
     *
     * @return string
     */
    protected function endSetup()
    {
        $this->progressBar->finish();

        $this->info('Installation of Harmony Platform eCommerce is now complete.');
        $this->comment('If you did not create admin user during installation, run `php artisan harmony:admin`.');
    }

    /**
     * Display the start setup script.
     *
     * @return string
     */
    protected function startSetup()
    {
        $this->info("
        ________________________________________________________________
                 _   _
                | | | | __ _ _ __ _ __ ___   ___  _ __  _   _
                | |_| |/ _` | '__| '_ ` _ \ / _ \| '_ \| | | |
                |  _  | (_| | |  | | | | | | (_) | | | | |_| |
                |_| |_|\__,_|_|  |_| |_| |_|\___/|_| |_|\__, |
                                                        |___/

                    Starting Installation. Please wait...
        ________________________________________________________________
        ");
    }

    protected function alterUsersTable()
    {
        $user = config('platform.user.model');

        try {
            $model = resolve($user);
        } catch (\Exception $e) {
            $model = null;
            $this->error("{$e->getMessage()} Model not found!");
        }

        if ($model !== null) {
            $table = $model->getTable();
            if (Schema::hasTable($table)) {
                Schema::table($table, function (Blueprint $table) {
                    $table->unsignedBigInteger('user_group_id')->nullable()->default(null);
                });
            }
        }
    }

    protected function executeCommand(string $command, array $parameters = [])
    {
        if (! $this->progressBar->getProgress()) {
            $this->progressBar->start();
        }

        $result = $this->call($command, $parameters);
        if ($result) {
            $parameters = http_build_query($parameters, '', ' ');
            $parameters = str_replace('%5C', '/', $parameters);

            $this->alert("An error has occured. The '{$command} {$parameters}' command was not executed.");
        }

        $this->progressBar->advance();

        // Visually slow down the installation process so that the user can read what's happening
        usleep(350000);
    }
}
