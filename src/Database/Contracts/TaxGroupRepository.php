<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\TaxGroup;
use Illuminate\Database\Eloquent\Collection;

interface TaxGroupRepository
{
    public function create(array $data): TaxGroup;

    public function all(): Collection;
}
