<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\AdminUser;

interface AdminUserRepository extends BaseContract
{
    /**
     * @param  string $email
     * @return \Harmony\Database\Models\AdminUser
     */
    public function findByEmail(string $email): AdminUser;
}
