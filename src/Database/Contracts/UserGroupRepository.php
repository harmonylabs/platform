<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\UserGroup;

interface UserGroupRepository extends BaseContract
{
    public function getIsDefault(): UserGroup;
}
