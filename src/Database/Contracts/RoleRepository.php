<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\Role;
use Illuminate\Support\Collection as SupportCollection;

interface RoleRepository extends BaseContract
{
    /**
     * @return \Harmony\Database\Models\Role
     */
    public function findAdminRole(): Role;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function options(): SupportCollection;
}
