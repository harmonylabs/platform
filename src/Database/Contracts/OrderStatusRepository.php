<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\OrderStatus;

interface OrderStatusRepository extends BaseContract
{
    public function findDefault(): OrderStatus;
}
