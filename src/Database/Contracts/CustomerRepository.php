<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\Customer;

interface CustomerRepository extends BaseContract
{
    public function findByEmail(string $email): ?Customer;
}
