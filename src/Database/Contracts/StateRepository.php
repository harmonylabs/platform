<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\State;
use Illuminate\Database\Eloquent\Collection;

interface StateRepository
{
    public function create(array $data): State;

    public function find(int $id): State;

    public function delete(int $id): int;

    public function all(): Collection;
}
