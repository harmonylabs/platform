<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\CategoryFilter;
use Illuminate\Database\Eloquent\Collection;

interface CategoryFilterRepository
{
    public function create(array $data): CategoryFilter;

    public function find(int $id): CategoryFilter;

    public function delete(int $id): int;

    public function all(): Collection;

    public function findByCategoryId(int $id): Collection;

    public function isCategoryFilterModelExists(int $categoryId, int $filterId, $type);
}
