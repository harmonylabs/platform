<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\Category;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as SupportCollection;

interface CategoryRepository extends BaseContract
{
    /**
     * @param  string $slug
     * @return \Harmony\Database\Models\Category
     */
    public function findBySlug(string $slug): Category;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function options(): SupportCollection;

    /**
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCategoryProducts(Request $request): Collection;

    /**
     * Get all the categories option to be used by the menu builder
     *
     * @return \Illuminate\Support\Collection
     */
    public function getCategoryOptionForMenuBuilder(): SupportCollection;
}
