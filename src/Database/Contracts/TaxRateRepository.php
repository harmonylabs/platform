<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\TaxRate;
use Illuminate\Database\Eloquent\Collection;

interface TaxRateRepository
{
    public function create(array $data): TaxRate;

    public function all(): Collection;
}
