<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\Menu;
use Illuminate\Database\Eloquent\Collection;

interface MenuRepository
{
    public function create(array $data): Menu;

    public function find(int $id): Menu;

    public function delete(int $id): int;

    public function all(): Collection;
}
