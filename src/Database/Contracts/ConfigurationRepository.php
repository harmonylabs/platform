<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\Configuration;

interface ConfigurationRepository
{
    /**
     * @param  array  $data
     * @return \Harmony\Database\Models\Configuration
     */
    public function create(array $data): Configuration;

    /**
     * Get model of a configuration by given code
     *
     * @param  string $code
     * @return string
     */
    public function getModelByCode($code);

    /**
     * Get the value of a configuration by given code
     *
     * @param  string $code
     * @return string
     */
    public function getValueByCode($code);
}
