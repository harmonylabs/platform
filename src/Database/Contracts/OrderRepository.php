<?php

namespace Harmony\Database\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface OrderRepository extends BaseContract
{
    public function findByUserId(int $id): Collection;

    public function getCurrentMonthTotalOrder(): int;

    public function getCurrentMonthTotalRevenue(): float;
}
