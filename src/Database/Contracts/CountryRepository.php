<?php

namespace Harmony\Database\Contracts;

use Illuminate\Support\Collection;

interface CountryRepository
{
    public function options(): Collection;
}
