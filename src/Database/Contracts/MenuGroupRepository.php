<?php

namespace Harmony\Database\Contracts;

use Illuminate\Support\Collection;

interface MenuGroupRepository extends BaseContract
{
    public function getTreeByIdentifier(string $identifier): Collection;
}
