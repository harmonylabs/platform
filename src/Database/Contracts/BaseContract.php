<?php

namespace Harmony\Database\Contracts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface BaseContract
{
    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(): Builder;

    /**
     * @param  array  $data
     * @return \Harmony\Database\Models\BaseModel
     */
    public function create(array $data);

    /**
     * @param  int  $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find(int $id);

    /**
     * @param  int $id
     * @return int
     */
    public function delete(int $id): int;

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all(): Collection;

    /**
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate(): LengthAwarePaginator;
}
