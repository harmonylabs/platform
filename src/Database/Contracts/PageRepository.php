<?php

namespace Harmony\Database\Contracts;

interface PageRepository extends BaseContract
{
    /**
     * @param  string $slug
     * @return \Harmony\Database\Models\Page
     */
    public function findBySlug(string $slug);
}
