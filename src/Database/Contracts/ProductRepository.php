<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\Product;

interface ProductRepository
{
    public function findBySlug(string $slug): Product;

    public function findByBarcode(string $barcode): Product;

    // public function getAllWithoutVariation(int $perPage): LengthAwarePaginator;
}
