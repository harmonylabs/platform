<?php

namespace Harmony\Database\Contracts;

use Harmony\Database\Models\Address;
use Illuminate\Database\Eloquent\Collection;

interface AddressRepository
{
    public function create(array $data): Address;

    public function find(int $id): Address;

    public function delete(int $id): int;

    public function all(): Collection;

    public function getByUserId(int $userId): Collection;
}
