<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\CustomerRepository as CustomerRepositoryContract;
use Harmony\Database\Models\Customer;

class CustomerRepository extends BaseRepository implements CustomerRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new Customer();
    }

    public function model(): Customer
    {
        return $this->model;
    }

    public function findByEmail(string $email): ?Customer
    {
        return Customer::whereEmail($email)->first();
    }
}
