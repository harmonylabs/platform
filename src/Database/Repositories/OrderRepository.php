<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\OrderRepository as OrderRepositoryContract;
use Harmony\Database\Models\Order;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

class OrderRepository extends BaseRepository implements OrderRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new Order();
    }

    public function model(): Order
    {
        return $this->model;
    }

    public function findByUserId(int $id): Collection
    {
        return Order::whereUserId($id)->get();
    }

    public function getCurrentMonthTotalOrder(): int
    {
        $firstDay = $this->getFirstDay();
        $totalOrder = Order::select('*')->where('created_at', '>', $firstDay)->count();

        return $totalOrder;
    }

    public function getCurrentMonthTotalRevenue(): float
    {
        $total = 0;
        $firstDay = $this->getFirstDay();

        $orders = Order::with('products')->select('*')->where('created_at', '>', $firstDay)->get();
        foreach ($orders as $order) {
            foreach ($order->products as $product) {
                $total += ($product->qty * $product->price) + $product->tax_amount;
            }
        }

        return $total;
    }

    protected function getFirstDay()
    {
        $startDay = Carbon::now();

        return $startDay->firstOfMonth();
    }
}
