<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\AddressRepository as AddressRepositoryContract;
use Harmony\Database\Models\Address;
use Illuminate\Database\Eloquent\Collection;

class AddressRepository implements AddressRepositoryContract
{
    public function create(array $data): Address
    {
        return Address::create($data);
    }

    public function find(int $id): Address
    {
        return Address::find($id);
    }

    public function delete(int $id): int
    {
        return Address::destroy($id);
    }

    public function all(): Collection
    {
        return Address::all();
    }

    public function getByUserId(int $userId): Collection
    {
        return Address::whereUserId($userId)->get();
    }
}
