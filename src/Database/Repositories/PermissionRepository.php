<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\PermissionRepository as PermissionRepositoryContract;
use Harmony\Database\Models\Permission;
use Illuminate\Database\Eloquent\Collection;

class PermissionRepository implements PermissionRepositoryContract
{
    public function create(array $data): Permission
    {
        return Permission::create($data);
    }

    public function findByName(string $name)
    {
        return Permission::whereName($name)->first();
    }

    public function find(int $id): Permission
    {
        return Permission::find($id);
    }

    public function all(): Collection
    {
        return Permission::all();
    }
}
