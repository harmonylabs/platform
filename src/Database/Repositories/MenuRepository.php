<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\MenuRepository as MenuRepositoryContract;
use Harmony\Database\Models\Menu;
use Illuminate\Database\Eloquent\Collection;

class MenuRepository implements MenuRepositoryContract
{
    public function create(array $data): Menu
    {
        return Menu::create($data);
    }

    public function find(int $id): Menu
    {
        return Menu::find($id);
    }

    public function delete(int $id): int
    {
        return Menu::destroy($id);
    }

    public function all(): Collection
    {
        return Menu::all();
    }
}
