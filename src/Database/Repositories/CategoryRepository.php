<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\CategoryRepository as CategoryRepositoryContract;
use Harmony\Database\Models\Category;
use Illuminate\Support\Collection as SupportCollection;

class CategoryRepository extends BaseRepository implements CategoryRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new Category();
    }

    public function model(): Category
    {
        return $this->model;
    }

    public function options(): SupportCollection
    {
        return Category::all()->pluck('name', 'id');
    }

    public function findBySlug(string $slug): Category
    {
        return Category::whereSlug($slug)->first();
    }
}
