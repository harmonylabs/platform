<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\AttributeRepository as AttributeRepositoryContract;
use Harmony\Database\Models\Attribute;

class AttributeRepository extends BaseRepository implements AttributeRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new Attribute();
    }

    public function model(): Attribute
    {
        return $this->model;
    }
}
