<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\PermissionRepository;
use Harmony\Database\Contracts\RoleRepository as RoleRepositoryContract;
use Harmony\Database\Models\Role;
use Illuminate\Support\Collection as SupportCollection;

class RoleRepository extends BaseRepository implements RoleRepositoryContract
{
    /**
     * @var \Harmony\Database\Models\Role
     */
    protected $model;

    public function __construct()
    {
        $this->model = new Role();
    }

    public function model()
    {
        return $this->model;
    }

    /**
     * @return \Harmony\Database\Models\Role
     */
    public function findAdminRole(): Role
    {
        return Role::whereName(Role::ADMIN)->first();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function options(): SupportCollection
    {
        return Role::all()->pluck('name', 'id');
    }

    /**
     * Save the role permission for the user.
     *
     * @param  \Harmony\Http\Requests\RoleRequest  $request
     * @param  \Harmony\Database\Models\Role  $role
     * @return void
     */
    protected function saveRolePermissions($request, $role)
    {
        $collection = SupportCollection::make([]);

        if ($request->get('permissions') !== null && count($request->get('permissions')) > 0) {
            foreach ($request->get('permissions') as $key => $value) {
                if ($value != 1) {
                    continue;
                }

                $permissions = explode(',', $key);
                foreach ($permissions as $name) {
                    $permissionRepository = app(PermissionRepository::class);
                    $permissionModel = $this->permissionRepository->findByName($name);

                    if ($permissionModel === null) {
                        $permissionModel = $this->permissionRepository->create(['name' => $name]);
                    }

                    $collection->push($permissionModel->id);
                }
            }

            $ids = $collection->unique();

            $role->permissions()->sync($ids);
        }
    }
}
