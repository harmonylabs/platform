<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\LanguageRepository as LanguageRepositoryContract;
use Harmony\Database\Models\Language;

class LanguageRepository extends BaseRepository implements LanguageRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new Language();
    }

    public function model(): Language
    {
        return $this->model;
    }
}
