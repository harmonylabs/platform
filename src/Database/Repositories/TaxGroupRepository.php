<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\TaxGroupRepository as TaxGroupRepositoryContract;
use Harmony\Database\Models\TaxGroup;
use Illuminate\Database\Eloquent\Collection;

class TaxGroupRepository implements TaxGroupRepositoryContract
{
    public function create(array $data): TaxGroup
    {
        return TaxGroup::create($data);
    }

    public function all(): Collection
    {
        return TaxGroup::all();
    }
}
