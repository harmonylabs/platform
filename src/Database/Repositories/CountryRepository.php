<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\CountryRepository as CountryRepositoryContract;
use Harmony\Database\Models\Country;
use Illuminate\Support\Collection;

class CountryRepository implements CountryRepositoryContract
{
    public function options(): Collection
    {
        return Country::all()->pluck('name', 'id');
    }

    public function currencyCodeOptions(): Collection
    {
        return Country::all()->pluck('currency_code', 'currency_code');
    }

    public function currencySymbolOptions(): Collection
    {
        return Country::all()->pluck('currency_symbol')->unique();
    }
}
