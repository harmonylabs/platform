<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\UserGroupRepository as UserGroupRepositoryContract;
use Harmony\Database\Models\UserGroup;

class UserGroupRepository extends BaseRepository implements UserGroupRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new UserGroup();
    }

    public function model(): UserGroup
    {
        return $this->model;
    }

    public function getIsDefault(): UserGroup
    {
        return UserGroup::whereIsDefault(true)->first();
    }
}
