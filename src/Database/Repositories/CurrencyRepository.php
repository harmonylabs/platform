<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\CurrencyRepository as CurrencyRepositoryContract;
use Harmony\Database\Models\Currency;
use Illuminate\Database\Eloquent\Collection;

class CurrencyRepository extends BaseRepository implements CurrencyRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new Currency();
    }

    public function model(): Currency
    {
        return $this->model;
    }

    public function create(array $data): Currency
    {
        return Currency::create($data);
    }

    public function find(int $id): Currency
    {
        return Currency::find($id);
    }

    public function delete(int $id): int
    {
        return Currency::destroy($id);
    }

    public function all(): Collection
    {
        return Currency::all();
    }
}
