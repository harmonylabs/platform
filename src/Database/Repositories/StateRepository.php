<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\StateRepository as StateRepositoryContract;
use Harmony\Database\Models\State;
use Illuminate\Database\Eloquent\Collection;

class StateRepository implements StateRepositoryContract
{
    public function create(array $data): State
    {
        return State::create($data);
    }

    public function find(int $id): State
    {
        return State::find($id);
    }

    public function delete(int $id): int
    {
        return State::destroy($id);
    }

    public function all(): Collection
    {
        return State::all();
    }
}
