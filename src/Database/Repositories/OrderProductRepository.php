<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\OrderProductRepository as OrderProductRepositoryContract;
use Harmony\Database\Models\OrderProduct;
use Harmony\Http\Events\OrderProductCreated;

class OrderProductRepository extends BaseRepository implements OrderProductRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new OrderProduct();
    }

    public function model(): OrderProduct
    {
        return $this->model;
    }

    public function create(array $data): OrderProduct
    {
        $orderProduct = parent::create($data);

        event(new OrderProductCreated($orderProduct));

        return $orderProduct;
    }
}
