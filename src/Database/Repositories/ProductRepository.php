<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\ProductRepository as ProductRepositoryContract;
use Harmony\Database\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductRepository extends BaseRepository implements ProductRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new Product();
    }

    public function model()
    {
        return $this->model;
    }

    public function find(int $id): Product
    {
        return Product::find($id);
    }

    public function all(): Collection
    {
        return Product::all();
    }

    public function delete(int $id): int
    {
        return Product::destroy($id);
    }

    public function getAllWithoutVariation($perPage = 10): LengthAwarePaginator
    {
        return Product::withoutVariation()->paginate($perPage);
    }

    public function findBySlug(string $slug): Product
    {
        return Product::whereSlug($slug)->first();
    }

    public function findByBarcode(string $barcode): Product
    {
        return Product::whereBarcode($barcode)->first();
    }
}
