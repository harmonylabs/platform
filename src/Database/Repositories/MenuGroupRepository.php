<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\MenuGroupRepository as MenuGroupRepositoryContract;
use Harmony\Database\Models\MenuGroup;
use Illuminate\Support\Collection;

class MenuGroupRepository extends BaseRepository implements MenuGroupRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new MenuGroup();
    }

    public function model(): MenuGroup
    {
        return $this->model;
    }

    public function getTreeByIdentifier(string $identifier): Collection
    {
        $menus = collect();

        $menuGroup = MenuGroup::whereIdentifier($identifier)->first();
        if ($menuGroup !== null) {
            $modelMenus = $menuGroup->menus()->whereNull('parent_id')->get();
            foreach ($modelMenus as $modelMenu) {
                $modelMenu->subMenus;
                $menus->push($modelMenu);
            }
        }

        return $menus;
    }
}
