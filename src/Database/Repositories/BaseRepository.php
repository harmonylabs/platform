<?php

namespace Harmony\Database\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class BaseRepository
{
    abstract public function model();

    public function paginate($perPage = 10): LengthAwarePaginator
    {
        return $this->model()->paginate($perPage);
    }

    public function query(): Builder
    {
        return $this->model()->query();
    }

    public function create(array $data)
    {
        return $this->model()->create($data);
    }

    public function find(int $id)
    {
        return $this->model()->find($id);
    }

    public function delete(int $id): int
    {
        return $this->model()->delete($id);
    }

    public function all(): Collection
    {
        return $this->model()->all();
    }
}
