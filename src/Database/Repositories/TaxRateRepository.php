<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\TaxRateRepository as TaxRateRepositoryContract;
use Harmony\Database\Models\TaxRate;
use Illuminate\Database\Eloquent\Collection;

class TaxRateRepository implements TaxRateRepositoryContract
{
    public function create(array $data): TaxRate
    {
        return TaxRate::create($data);
    }

    public function all(): Collection
    {
        return TaxRate::all();
    }
}
