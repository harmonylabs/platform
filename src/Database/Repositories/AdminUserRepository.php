<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\AdminUserRepository as AdminUserRepositoryContract;
use Harmony\Database\Models\AdminUser;

class AdminUserRepository extends BaseRepository implements AdminUserRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new AdminUser();
    }

    public function model(): AdminUser
    {
        return $this->model;
    }

    public function findByEmail(string $email): AdminUser
    {
        return AdminUser::whereEmail($email)->first();
    }
}
