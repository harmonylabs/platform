<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\CategoryFilterRepository as CategoryFilterRepositoryContract;
use Harmony\Database\Models\CategoryFilter;
use Illuminate\Database\Eloquent\Collection;

class CategoryFilterRepository implements CategoryFilterRepositoryContract
{
    public function create(array $data): CategoryFilter
    {
        return CategoryFilter::create($data);
    }

    public function find(int $id): CategoryFilter
    {
        return CategoryFilter::find($id);
    }

    public function delete(int $id): int
    {
        return CategoryFilter::destroy($id);
    }

    public function all(): Collection
    {
        return CategoryFilter::all();
    }

    public function findByCategoryId(int $id): Collection
    {
        return CategoryFilter::whereCategoryId($id)->get();
    }

    public function isCategoryFilterModelExists(int $categoryId, int $filterId, $type)
    {
        $model = CategoryFilter::whereCategoryId($categoryId)
                                ->whereFilterId($filterId)
                                ->whereType($type)
                                ->first();

        if ($model !== null) {
            return true;
        }

        return false;
    }
}
