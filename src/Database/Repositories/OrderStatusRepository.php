<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\OrderStatusRepository as OrderStatusRepositoryContract;
use Harmony\Database\Models\OrderStatus;

class OrderStatusRepository extends BaseRepository implements OrderStatusRepositoryContract
{
    protected $model;

    public function __construct()
    {
        $this->model = new OrderStatus();
    }

    public function model(): OrderStatus
    {
        return $this->model;
    }

    public function findDefault(): OrderStatus
    {
        return OrderStatus::whereIsDefault(1)->first();
    }
}
