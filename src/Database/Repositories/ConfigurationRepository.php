<?php

namespace Harmony\Database\Repositories;

use Harmony\Database\Contracts\ConfigurationRepository as ConfigurationRepositoryContract;
use Harmony\Database\Models\Configuration;

class ConfigurationRepository implements ConfigurationRepositoryContract
{
    /**
     * {@inheritDoc}
     */
    public function create(array $data): Configuration
    {
        return Configuration::create($data);
    }

    /**
     * {@inheritDoc}
     */
    public function getModelByCode($code)
    {
        $configuration = Configuration::whereCode($code)->first();
        if ($configuration === null) {
            return;
        }

        return $configuration;
    }

    /**
     * {@inheritDoc}
     */
    public function getValueByCode($code)
    {
        $configuration = Configuration::whereCode($code)->first();
        if ($configuration === null) {
            return;
        }

        return $configuration->value;
    }
}
