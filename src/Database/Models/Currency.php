<?php

namespace Harmony\Database\Models;

class Currency extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'symbol', 'conversion_rate', 'status',
    ];
}
