<?php

namespace Harmony\Database\Models;

use Harmony\Http\Notifications\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\HasApiTokens;

class Customer extends BaseModel
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'image_path',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The accessors to append to the models array form.
     *
     * @var array
     */
    protected $appends = [
        'image_path_url ', 'image_path_name',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function getFullNameAttribute()
    {
        return $this->attributes['first_name'].' '.$this->attributes['last_name'];
    }

    public function getImagePathUrlAttribute()
    {
        return asset('storage/'.$this->attributes['image_path']);
    }

    public function getImagePathNameAttribute()
    {
        return basename($this->image_path);
    }

    public function setPasswordAttribute($val)
    {
        $this->attributes['password'] = bcrypt($val);
    }

    public function hasPermission($routeName): bool
    {
        if ($this->is_super_admin) {
            return true;
        }

        $role = $this->role;
        if ($role->permissions->pluck('name')->contains($routeName) == false) {
            return false;
        }

        return true;
    }

    public function getPassportClient()
    {
        $client = $this->clients->first();
        if (null === $client) {
            $clientRepository = app(ClientRepository::class);
            $redirectUri = asset('');
            $client = $clientRepository->createPasswordGrantClient($this->id, $this->full_name, $redirectUri);
        }

        return $client;
    }

    public function validateForPassportPasswordGrant($password)
    {
        return Hash::check($password, $this->password);
    }

    public function permissions()
    {
        dd($this->role->permissions);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
