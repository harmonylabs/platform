<?php

namespace Harmony\Database\Models;

class Page extends BaseModel
{
    /**
     * The widget content tags
     *
     * @var array
     */
    protected $contentTags = ['%%%', '%%%'];

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'content', 'meta_title', 'meta_description'
    ];

    /**
     * Get the content and make sure it has widget key then render the widget too
     *
     * @return string
     */
    public function getContent()
    {
        $content = $this->content;
        $pattern = sprintf('/(@)?%s\s*(.+?)\s*%s(\r?\n)?/s', $this->contentTags[0], $this->contentTags[1]);
        $callback = function ($matches) {
            $whitespace = empty($matches[3]) ? '' : $matches[3] . $matches[3];
        };

        return preg_replace_callback($pattern, $callback, $content);
    }
}
