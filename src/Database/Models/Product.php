<?php

namespace Harmony\Database\Models;

class Product extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'type', 'sku', 'barcode', 'description', 'status', 'in_stock',
        'track_stock', 'qty', 'is_taxable', 'price', 'cost_price', 'weight', 'height',
        'width', 'length', 'meta_title', 'meta_description',
    ];

    const TAX_CONFIG_KEY = 'tax_percentage';

    const PRODUCT_BASIC = 'BASIC';
    const PRODUCT_DOWNLOADABLE = 'DOWNLOADABLE';
    const PRODUCT_VARIABLE = 'VARIABLE_PRODUCT';

    const PRODUCT_TYPES = [
        self::PRODUCT_BASIC => 'Basic',
        self::PRODUCT_DOWNLOADABLE => 'Downloadable',
        self::PRODUCT_VARIABLE => 'Variable Product',
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function getPrice($format = true)
    {
        $price = $this->price;

        if ($format) {
            return number_format($price, 2);
        }

        return $price;
    }

    public function getQty()
    {
        $qty = $this->qty;
        if ($qty <= 0) {
            return __('Not in stock');
        }

        return __(':qty in stock', ['qty' => number_format($qty, 0)]);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function mainImage()
    {
        return $this->hasOne(ProductImage::class)->whereIsMainImage(true);
    }

    public function getMainImageUrlAttribute(): string
    {
        $default = 'https://placehold.it/250x250';
        $image = $this->images()->whereIsMainImage(true)->first();
        if ($image === null) {
            return $default;
        }

        return asset('storage/'.$image->path);
    }

    public function getAttributeById($attributeId): Attribute
    {
        return Attribute::find($attributeId);
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class);
    }

    /**
     * Scope a query to only include popular users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutVariation($query)
    {
        return $query->where('type', '!=', 'VARIATION');
    }
}
