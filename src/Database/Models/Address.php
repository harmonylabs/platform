<?php

namespace Harmony\Database\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'customer_id',
        'company_name',
        'first_name',
        'last_name',
        'address1',
        'address2',
        'postcode',
        'city',
        'state',
        'country_id',
        'phone',
    ];

    const OPTION_TYPE = [
        'SHIPPING' => 'Shipping Address',
        'BILLING' => 'Billing Address',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
