<?php

namespace Harmony\Database\Models;

class Role extends BaseModel
{
    /** @var string */
    const ADMIN = 'Administrator';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];

    /**
     * @param  string $routes
     * @return bool
     */
    public function hasPermission($routes)
    {
        $modelPermissions = $this->permissions->pluck('name');
        $permissions = explode(',', $routes);
        $hasPermission = true;

        foreach ($permissions as $permissions) {
            if (! $modelPermissions->contains($permissions)) {
                $hasPermission = false;
            }
        }

        return $hasPermission;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
