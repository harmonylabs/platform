<?php

namespace Harmony\Database\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url', 'sort_order', 'menu_group_id', 'parent_id',
    ];

    public function subMenus()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
}
