<?php

namespace Harmony\Database\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeDropdownOption extends Model
{
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'attribute_id', 'display_text', 'path'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
}
