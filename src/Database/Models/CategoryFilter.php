<?php

namespace Harmony\Database\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryFilter extends Model
{
    /** @var string */
    public const PROPERTY_FILTER_TYPE = 'PROPERTY';

    /** @var string */
    public const ATTRIBUTE_FILTER_TYPE = 'ATTRIBUTE';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'filter_id', 'type',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
