<?php

namespace Harmony\Database\Models;

class Order extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shipping_option', 'payment_option', 'order_status_id', 'currency_id',
        'customer_id', 'shipping_address_id', 'billing_address_id', 'track_code',
    ];

    public function orderStatus()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function user()
    {
        return $this->belongsTo(Customer::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function shippingAddress()
    {
        return $this->belongsTo(Address::class, 'shipping_address_id');
    }

    public function billingAddress()
    {
        return $this->belongsTo(Address::class, 'billing_address_id');
    }

    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }
}
