<?php

namespace Harmony\Database\Models;

class Attribute extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'display_as',
    ];

    /**
     * The accessors to append to the models array form.
     *
     * @var array
     */
    protected $appends = [
        'dropdown',
    ];

    const DISPLAY_AS = [
        'IMAGE' => 'Image',
        'Text' => 'Text',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
