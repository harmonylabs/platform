<?php

namespace Harmony\Database\Models;

class MenuGroup extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'identifier',
    ];

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }
}
