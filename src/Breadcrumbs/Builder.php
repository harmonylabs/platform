<?php

namespace Harmony\Breadcrumbs;

use Illuminate\Support\Collection;

class Builder
{
    /**
     * The collection instance.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $collection;

    /**
     * Create a new builder instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->collection = new Collection();
    }

    /**
     * Create a breadcrumb instance in the collection by name.
     *
     * @param  mixed  $name
     * @param  callable  $callable
     * @return \Harmony\Breadcrumbs\Builder
     */
    public function make($name, callable $callable)
    {
        $breadcrumb = new Factory($callable);
        $breadcrumb->route($name);

        $this->collection->put($name, $breadcrumb);
    }

    /**
     * Get an breadcrumb instance from the collection by key.
     *
     * @param  string  $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->collection->get($key);
    }

    /**
     * Render the string contents of the breadcrumb.
     *
     * @param  string  $routeName
     * @return string|\Illuminate\View\View
     */
    public function render($routeName)
    {
        $breadcrumb = $this->collection->get($routeName);

        if (null === $breadcrumb) {
            return '';
        }

        return view('platform::breadcrumb.index')
             ->with(compact('breadcrumb'));
    }
}
