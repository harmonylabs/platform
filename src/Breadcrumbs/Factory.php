<?php

namespace Harmony\Breadcrumbs;

use Harmony\Contracts\Breadcrumb;
use Harmony\Support\Facades\Breadcrumb as Facade;
use Illuminate\Support\Collection;

class Factory implements Breadcrumb
{
    /**
     * The specified breadcrumb label.
     *
     * @var string|null
     */
    public $label = null;

    /**
     * The specified breadcrumb route.
     *
     * @var string|null
     */
    public $route = null;

    /**
     * The specified breadcrumb parent.
     *
     * @var \Illuminate\Support\Collection|null
     */
    public $parents = null;

    /**
     * Create a new factory instance.
     *
     * @param  callable  $callable
     * @return void
     */
    public function __construct($callable)
    {
        $this->parents = new Collection();

        $callable($this);
    }

    /**
     * Get / set specified breadcrumb label
     *
     * @param  string|null
     * @return mixed
     */
    public function label($label = null)
    {
        if (null === $label) {
            return $this->label;
        }

        $this->label = $label;

        return $this;
    }

    /**
     * Get / set the specified route
     *
     * @param  string|null
     * @return mixed
     */
    public function route($route = null)
    {
        if (null === $route) {
            return $this->route;
        }

        $this->route = $route;

        return $this;
    }

    /**
     * Set the parent route of the specified breadcrumb.
     *
     * @param  string  $key
     * @return \Harmony\Breadcrumbs\Factory
     */
    public function parent($key): self
    {
        $breadcrumb = Facade::get($key);

        $this->parents->put($key, $breadcrumb);

        return $this;
    }
}
